<header id="this-is-top">
	<div class="container-fluid">
		<div class="topmenu row">
			<nav class="col-sm-offset-3 col-md-offset-4 col-lg-offset-4 col-sm-6 col-md-6 col-lg-6">
				<a href="#">Test Preparation</a>
				<a href="#">Branch</a>
				<a href="#">Why Us ?</a>
				<a href="#">Downloads</a>
				<a href="#">Sign In</a>
				<a href="#">Sign Up</a>
			</nav>
			<nav class="text-right col-sm-3 col-md-2 col-lg-2">
				<a class="social-icons" href="#"><i class="fa fa-facebook"></i></a>
				<a class="social-icons" href="#"><i class="fa fa-google-plus"></i></a>
				<a class="social-icons" href="#"><i class="fa fa-twitter"></i></a>
				<a class="social-icons" href="#"><i class="fa fa-youtube"></i></a>
			</nav>
		</div>
		<div class="row header">
			<div class="col-sm-3 col-md-3 col-lg-3">
				<a href="index.php" id="logo"></a>
			</div>
			<div class="col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-sm-8 col-md-8 col-lg-8">
				<div class="text-right header-padding">
					<div class="h-block"><span>CALL US</span>+977-016923202</div>
					<div class="h-block"><span>EMAIL US</span>info@ktmimmigration.com</div>
					<div class="h-block"><span>WORKING HOURS</span>Sun - Fri  10.00 - 5.00</div>
					<a class="btn btn-success" href="#">Apply Online Now !</a>
				</div>
			</div> 
		</div>
		<div id="main-menu-bg"></div>  
		<a id="menu-open" href="#"><i class="fa fa-bars"></i></a> 
		<nav class="main-menu navbar-main-slide">
			<ul class="nav navbar-nav navbar-main">
				<li>
					<a href="index.php">HOME</a>
				</li>
				<li>
					<a href="about.php">ABOUT US</a>
				</li>
				<li class="dropdown">
					<a data-toggle="dropdown" class="dropdown-toggle border-hover-color1" href="05_services.html">OUR SERVICES <i class="fa fa-angle-down"></i></a>
					<ul class="dropdown-menu">
						<li><a href="#">Educational Loan Assistance</a></li>
						<li><a href="#">Visa Process</a></li>
						<li><a href="#">Documentation Guidance</a></li>
						<li><a href="#">Visa Lodgement</a></li>
						<li><a href="#">Expert Educational Counselling</a></li>
						<li><a href="#">Scholarship Assistance</a></li>
						<li><a href="#">Interview Assistance</a></li>
						<li><a href="#">Pre Departure Briefing</a></li>
						<li><a href="#">Travel Assistance</a></li>
					</ul>
				</li>
				<li><a href="countries.php">COUNTRY INFORMATION</a></li>
				<li><a href="countries.php">UNIVERSITY INFORMATION</a></li>
				<li><a href="#">CONTACT US</a></li>
				<li><a href="#">BLOG</a></li>			
				<li><a class="btn_header_search" href="#"><i class="fa fa-search"></i></a></li>
			</ul>
			<div class="search-form-modal transition">
				<form class="navbar-form header_search_form">
					<i class="fa fa-times search-form_close"></i>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Search">
					</div>
					<button type="submit" class="btn btn_search customBgColor">Search</button>
				</form>
			</div>
        </nav>
		<a id="menu-close" href="#"><i class="fa fa-times"></i></a> 
	</div>
</header>