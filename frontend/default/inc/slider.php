<div id="owl-main-slider" class="owl-carousel enable-owl-carousel" data-single-item="true" data-pagination="false" data-auto-play="true" data-main-slider="true" data-stop-on-hover="true">
	<div class="item">
		<img src="img/study_in_australia.jpg" alt="Img">
		<div class="container-fluid">
			<div class="slider-content col-md-6 col-lg-6">
				<div style="display:table;">
					<div style="display:table-cell; width:100px; vertical-align:top;">
						<a class="prev"><i class="fa fa-angle-left"></i></a>
						<a class="next"><i class="fa fa-angle-right"></i></a>
					</div>
					<div style="display:table-cell;">
						<h1>STUDY IN AUSTRALIA</h1>
					</div>
				</div>
				<p>Want to Study in Australia? Read expert advice on studying abroad in Australia including courses, costs, scholarships and visa information.</p>
				<a class="btn btn-success" href="#">LEARN MORE</a>
			</div>
		</div>
	</div>
	<div class="item">
		<img src="img/graduate_studets.jpg" alt="Img">
		<div class="container-fluid">
			<div class="slider-content col-md-6 col-lg-6">
				<div style="display:table;">
					<div style="display:table-cell; width:100px; vertical-align:top;">
						<a class="prev"><i class="fa fa-angle-left"></i></a>
						<a class="next"><i class="fa fa-angle-right"></i></a>
					</div>
					<div style="display:table-cell;">
						<h1>VARIOUS GRADUATE PROGRAMS</h1>
					</div>
				</div>
				<p>Nunc accumsan metus quis metus. Sed luctus. Mauris eu enim quisque dignissim nequesudm consectetuer dapibus wn eu leo integer varius erat.</p>
				<a class="btn btn-success" href="#">LEARN MORE</a>
			</div>
		</div>
	</div>
</div>