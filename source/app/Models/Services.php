<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class services extends Sximo  {
	
	protected $table = 'ktmimmig_services';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ktmimmig_services.* FROM ktmimmig_services  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ktmimmig_services.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
