<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class membershipdetailscopy extends Sximo  {
	
	protected $table = 'ktimmig_membership';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ktimmig_membership.* FROM ktimmig_membership  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ktimmig_membership.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
