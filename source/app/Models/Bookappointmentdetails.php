<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class bookappointmentdetails extends Sximo  {
	
	protected $table = 'ktmimmig_book_appointment_details';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ktmimmig_book_appointment_details.* FROM ktmimmig_book_appointment_details  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ktmimmig_book_appointment_details.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
