<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class branchdetails extends Sximo  {
	
	protected $table = 'tbl_branches';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tbl_branches.* FROM tbl_branches  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tbl_branches.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
