<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class studentregistrationdetails extends Sximo  {
	
	protected $table = 'tbl_student_details';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tbl_student_details.* FROM tbl_student_details  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tbl_student_details.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
