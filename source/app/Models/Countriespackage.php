<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class countriespackage extends Sximo  {
	
	protected $table = 'ktmimmig_countries_package';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ktmimmig_countries_package.* FROM ktmimmig_countries_package  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ktmimmig_countries_package.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
