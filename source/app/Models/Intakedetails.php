<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class intakedetails extends Sximo  {
	
	protected $table = 'ktmimmig_intake';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ktmimmig_intake.* FROM ktmimmig_intake  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ktmimmig_intake.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
