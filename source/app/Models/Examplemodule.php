<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class examplemodule extends Sximo  {
	
	protected $table = 'tb_module';
	protected $primaryKey = 'module_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_module.* FROM tb_module  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_module.module_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
