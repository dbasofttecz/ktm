<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class testpreparationlist extends Sximo  {
	
	protected $table = 'test_preparation';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT test_preparation.* FROM test_preparation  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE test_preparation.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
