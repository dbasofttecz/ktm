<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class ieltsclassbooking extends Sximo  {
	
	protected $table = 'ktmimmig_ielts_class_booking_details';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ktmimmig_ielts_class_booking_details.* FROM ktmimmig_ielts_class_booking_details  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ktmimmig_ielts_class_booking_details.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
