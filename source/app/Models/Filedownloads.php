<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class filedownloads extends Sximo  {
	
	protected $table = 'ktmimmig_downloads';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ktmimmig_downloads.* FROM ktmimmig_downloads  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ktmimmig_downloads.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
