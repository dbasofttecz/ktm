<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class recenttestscore extends Sximo  {
	
	protected $table = 'ktmimmig_recent_test_score';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ktmimmig_recent_test_score.* FROM ktmimmig_recent_test_score  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ktmimmig_recent_test_score.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
