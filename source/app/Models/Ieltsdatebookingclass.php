<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class ieltsdatebookingclass extends Sximo  {
	
	protected $table = 'ielts_date_book';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ielts_date_book.* FROM ielts_date_book  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ielts_date_book.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
