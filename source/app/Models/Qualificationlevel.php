<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class qualificationlevel extends Sximo  {
	
	protected $table = 'ktmimmig_qualification_level';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ktmimmig_qualification_level.* FROM ktmimmig_qualification_level  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ktmimmig_qualification_level.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
