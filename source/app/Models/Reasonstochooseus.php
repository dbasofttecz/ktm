<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class reasonstochooseus extends Sximo  {
	
	protected $table = 'ktmimmig_why_us';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ktmimmig_why_us.* FROM ktmimmig_why_us  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ktmimmig_why_us.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
