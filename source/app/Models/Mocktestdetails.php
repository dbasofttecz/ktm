<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class mocktestdetails extends Sximo  {
	
	protected $table = 'tbl_mock_test_dates';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tbl_mock_test_dates.* FROM tbl_mock_test_dates  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tbl_mock_test_dates.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
