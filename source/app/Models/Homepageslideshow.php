<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class homepageslideshow extends Sximo  {
	
	protected $table = 'ktmimmig_homepage_slideshow';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ktmimmig_homepage_slideshow.* FROM ktmimmig_homepage_slideshow  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ktmimmig_homepage_slideshow.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
