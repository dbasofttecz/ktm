<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class countrydetails extends Sximo  {
	
	protected $table = 'tbl_countries';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tbl_countries.* FROM tbl_countries  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tbl_countries.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
