<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Latestnewsnew extends Sximo  {
	
	protected $table = 'latest_news';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
	}

	public static function querySelect(  ){
		return "  SELECT latest_news.* FROM latest_news  ";
	}	

	public static function queryWhere(  ){
		return "  WHERE latest_news.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
