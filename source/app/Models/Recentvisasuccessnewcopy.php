<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class recentvisasuccessnewcopy extends Sximo  {
	
	protected $table = 'ktimmig_visa_success';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ktimmig_visa_success.* FROM ktimmig_visa_success  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ktimmig_visa_success.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
