<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class mocktestbookingdetails extends Sximo  {
	
	protected $table = 'mockTestBookingDetails';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT mockTestBookingDetails.* FROM mockTestBookingDetails  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE mockTestBookingDetails.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
