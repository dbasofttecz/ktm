<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class brancheslist extends Sximo  {
	
	protected $table = 'ktmimmig_branches';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ktmimmig_branches.* FROM ktmimmig_branches  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ktmimmig_branches.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
