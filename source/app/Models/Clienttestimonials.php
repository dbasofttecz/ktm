<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class clienttestimonials extends Sximo  {
	
	protected $table = 'ktmimmig_testimonials';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ktmimmig_testimonials.* FROM ktmimmig_testimonials  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ktmimmig_testimonials.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
