<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class universitylistcopy extends Sximo  {
	
	protected $table = 'universitylist';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT universitylist.* FROM universitylist  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE universitylist.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
