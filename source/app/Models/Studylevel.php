<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class studylevel extends Sximo  {
	
	protected $table = 'study_level';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT study_level.* FROM study_level  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE study_level.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
