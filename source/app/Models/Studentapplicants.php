<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class studentapplicants extends Sximo  {
	
	protected $table = 'tbl_applications';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tbl_applications.* FROM tbl_applications  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tbl_applications.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
