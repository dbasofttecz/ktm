<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class onlineapplicationsnewtwo extends Sximo  {
	
	protected $table = 'ktmimmig_apply_online';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ktmimmig_apply_online.* FROM ktmimmig_apply_online  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ktmimmig_apply_online.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
