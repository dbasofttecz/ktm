<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class noticeboardnew extends Sximo  {
	
	protected $table = 'ktmimmig_notice_board';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ktmimmig_notice_board.* FROM ktmimmig_notice_board  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ktmimmig_notice_board.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
