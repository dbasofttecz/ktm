<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class countrieCopy extends Sximo  {
	
	protected $table = 'ktmimmig_countries';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ktmimmig_countries.* FROM ktmimmig_countries  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ktmimmig_countries.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
