<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use Input;

class universityloginprocess extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
          $rules = array(
            'email'    => 'required|email',
            'password' => 'required',
        );
        if (CNF_RECAPTCHA == 'true') {
            $rules['captcha'] = 'required|captcha';
        }

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {

        

            if (\Auth::attempt(array('email' => $request->input('email'), 'password' => $request->input('password')))) {
                if (\Auth::check()) {
                    $row = User::find(\Auth::user()->id);
// dd($row);
                    if ($row->active == '0') {
                        // inactive
                        if ($request->ajax() == true) {
                            return response()->json(['status' => 'error', 'message' => 'Your Account is not active']);
                        } else {
                            \Auth::logout();
                            return Redirect::to('user/login')->with('message', \SiteHelpers::alert('error', 'Your Account is not active'));
                        }

                    } else if ($row->active == '2') {

                        if ($request->ajax() == true) {
                            return response()->json(['status' => 'error', 'message' => 'Your Account is BLocked']);
                        } else {
                            // BLocked users
                            \Auth::logout();
                            return Redirect::to('user/login')->with('message', \SiteHelpers::alert('error', 'Your Account is BLocked'));
                        }
                    } else {
                        \DB::table('tb_users')->where('id', '=', $row->id)->update(array('last_login' => date("Y-m-d H:i:s")));
                        \Session::put('uid', $row->id);
                        \Session::put('gid', $row->group_id);
                        \Session::put('eid', $row->email);
                        \Session::put('ll', $row->last_login);
                        \Session::put('fid', $row->first_name . ' ' . $row->last_name);
                        if (\Session::get('lang') == '') {
                            \Session::put('lang', CNF_LANG);
                        }

                        if ($request->ajax() == true) {
                            if (CNF_FRONT == 'false'):
                                return response()->json(['status' => 'success', 'url' => url('univeristydashboard')]);
                            else:
                                return response()->json(['status' => 'success', 'url' => url('')]);
                            endif;

                        } else {
                            if (CNF_FRONT == 'true'):
                                return Redirect::to('univeristydashboard');
                            else:
                                return Redirect::to('/');
                            endif;

                        }

                    }

                }

            } else {

                if ($request->ajax() == true) {
                    return response()->json(['status' => 'error', 'message' => 'Your username/password combination was incorrect']);
                } else {

                    return Redirect::to('studentLogin')
                        ->with('message', \SiteHelpers::alert('error', 'Your username/password combination was incorrect'))
                        ->withInput();
                }

            }
        } else {

            if ($request->ajax() == true) {
                return response()->json(['status' => 'error', 'message' => 'The following  errors occurred']);
            } else {

                return Redirect::to('universitylogin')
                    ->with('message', \SiteHelpers::alert('error', 'The following  errors occurred'))
                    ->withErrors($validator)->withInput();
            }

        }



    }

   
}
