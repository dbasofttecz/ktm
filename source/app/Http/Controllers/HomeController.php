<?php namespace App\Http\Controllers;

use App\Models\countries;
use App\Models\Countriespackage;
use App\Models\Latestnewsnew;
use App\Models\recentvisasuccessnew;
use App\Models\Testpreparationlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Input;
use Mail;
use Redirect;
use Validator;

class HomeController extends Controller
{

    private $data;

    public function __construct()
    {
        parent::__construct();

        \App::setLocale(CNF_LANG);
        if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

            $lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
            \App::setLocale($lang);
        }

        $this->data['pageLang'] = 'en';
        if (\Session::get('lang') != '') {
            $this->data['pageLang'] = \Session::get('lang');
        }

        View::share('our_services', DB::table('ktmimmig_services')->get());
        View::share('branches', DB::table('ktmimmig_branches')->get());
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */

    public function index(Request $request)
    {
        if (CNF_FRONT == 'false' && $request->segment(1) == ''):
            return Redirect::to('dashboard');
        endif;

        $page = $request->segment(1);
        if ($page != ''):
            $content = \DB::table('tb_pages')->where('alias', '=', $page)->where('status', '=', 'enable')->get();

            if (count($content) >= 1) {

                $row                        = $content[0];
                $this->data['pageTitle']    = $row->title;
                $this->data['pageNote']     = $row->note;
                $this->data['pageMetakey']  = ($row->metakey != '' ? $row->metakey : CNF_METAKEY);
                $this->data['pageMetadesc'] = ($row->metadesc != '' ? $row->metadesc : CNF_METADESC);

                $this->data['breadcrumb'] = 'active';

                if ($row->access != '') {
                    $access = json_decode($row->access, true);
                } else {
                    $access = array();
                }

                // If guest not allowed
                if ($row->allow_guest != 1) {
                    $group_id = \Session::get('gid');
                    $isValid  = (isset($access[$group_id]) && $access[$group_id] == 1 ? 1 : 0);
                    if ($isValid == 0) {
                        return Redirect::to('')
                            ->with('message', \SiteHelpers::alert('error', Lang::get('core.note_restric')));
                    }
                }

                if (file_exists(base_path() . '/resources/views/layouts/' . CNF_THEME . '/template/' . $row->filename . '.blade.php') && $row->filename != '') {
                    $page_template = 'layouts.' . CNF_THEME . '.template.' . $row->filename;
                } else {
                    $page_template = 'layouts.' . CNF_THEME . '.template.page';
                }

                $this->data['content']  = \PostHelpers::formatContent($row->note);
                $this->data['filename'] = $row->filename;

                //    echo '<pre>';print_r($this->data);echo '</pre>'; exit;
                if ($row->template == 'backend') {
                    $this->data['pageNote'] = 'View';

                    return view($page_template, $this->data);
                } else {

                    $this->data['pages'] = $page_template;
                    $page                = 'layouts.' . CNF_THEME . '.index';

                    return view($page, $this->data);
                }

            } else {
                return Redirect::to('')
                    ->with('message', \SiteHelpers::alert('error', \Lang::get('core.note_noexists')));
            } else :

            $sql = \DB::table('tb_pages')->where('default', 1)->get();
            if (count($sql) >= 1) {
                $this->data['testimonialList']            = DB::table('ktmimmig_testimonials')->get();
                $this->data['noticeBoardList']            = DB::table('ktmimmig_notice_board')->get();
                $this->data['whyChooseUs']                = DB::table('ktmimmig_why_us')->get();
                $this->data['latest_news']                = DB::table('latest_news')->get();
                $this->data['ktmimmig_recent_test_score'] = DB::table('ktmimmig_recent_test_score')->get();
                $this->data['ktimmig_visa_success']       = DB::table('ktimmig_visa_success')->get();
                $this->data['membership']                 = DB::table('ktimmig_membership')->orderBy('organizationName', 'asc')->get();
                $this->data['universitylist']             = DB::table('universitylist')->orderBy('universityName', 'asc')->get();
                $this->data['countriesList']              = DB::table('ktmimmig_countries')->get();
                $this->data['courseLevelList']            = DB::table('course_types')->get();
                $this->data['studyLevelList']             = DB::table('study_level')->get();
                $this->data['alllistrecord']              = DB::table('universitylistdetails')->get();
                $this->data['homePageSlideshow']          = DB::table('ktmimmig_homepage_slideshow')->get();
                $this->data['display_illets']             = DB::table('test_preparation')->get();
                $row                                      = $sql[0];

                $this->data['pageTitle']    = $row->title;
                $this->data['pageNote']     = $row->note;
                $this->data['breadcrumb']   = 'inactive';
                $this->data['pageMetakey']  = $row->metakey;
                $this->data['pageMetadesc'] = $row->metadesc;
                $this->data['filename']     = $row->filename;

                if (file_exists(base_path() . '/resources/views/layouts/' . CNF_THEME . '/template/' . $row->filename . '.blade.php') && $row->filename != '') {
                    $page_template = 'layouts.' . CNF_THEME . '.template.' . $row->filename;
                } else {
                    $page_template = 'layouts.' . CNF_THEME . '.template.page';
                }

                $this->data['pages']   = $page_template;
                $this->data['content'] = \PostHelpers::formatContent($row->note);
                $page                  = 'layouts.' . CNF_THEME . '.index';

                return view($page, $this->data);

            } else {

                return ' No Default page set up !';
            }

        endif;

    }

    public function getLang($lang = 'en')
    {
        \Session::put('lang', $lang);

        return Redirect::back();
    }

    public function getSkin($skin = 'sximo')
    {
        \Session::put('themes', $skin);

        return Redirect::back();
    }

    public function postContact(Request $request)
    {

        $this->beforeFilter('csrf', array('on' => 'post'));
        $rules = array(
            'name'    => 'required',
            'subject' => 'required',
            'message' => 'required|min:20',
            'sender'  => 'required|email',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {

            $data = array(
                'name'    => $request->input('name'),
                'sender'  => $request->input('sender'),
                'subject' => $request->input('subject'),
                'notes'   => $request->input('message'),
            );
            $message    = view('emails.contact', $data);
            $data['to'] = CNF_EMAIL;
            if (defined('CNF_MAIL') && CNF_MAIL == 'swift') {
                Mail::send('user.emails.contact', $data, function ($message) use ($data) {
                    $message->to($data['to'])->subject($data['subject']);
                });

            } else {

                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: ' . $request->input('name') . ' <' . $request->input('sender') . '>' . "\r\n";
                //mail($data['to'],$data['subject'], $message, $headers);
            }

            return Redirect::to($request->input('redirect'))->with('message', \SiteHelpers::alert('success', 'Thank You , Your message has been sent !'));

        } else {
            return Redirect::to($request->input('redirect'))->with('message', \SiteHelpers::alert('error', 'The following errors occurred'))
                ->withErrors($validator)->withInput();
        }
    }

    public static function getSingleCountryDetails(Request $request)
    {
        $model  = new Countries();
        $model1 = new Countriespackage();
        $info   = $model1::makeInfo('countries');
        $data   = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',
        );
        $page   = isset($_GET['page']) ? $_GET['page'] : 1;
        $params = array(
            'page'   => $page,
            'limit'  => (isset($_GET['rows']) ? filter_var($_GET['rows'], FILTER_VALIDATE_INT) : 10),
            'sort'   => 'id',
            'order'  => 'asc',
            'params' => '',
            'global' => 1,
        );
        $data['countryId']    = $countryId    = base64_decode($request->route('countryId'));
        $data['banner_image'] = DB::select('select bannerImage from ktmimmig_countries where id = ' . $countryId);
        $data['rowData']      = DB::table('ktmimmig_countries_package')->where('countryId', $countryId)->orderBy('id', 'asc')->get();

        $data['newsData'] = DB::table('latest_news')->where('country', $countryId)->get();
        $result           = $model::getRows($params);
        $data['rowData1'] = $result['rows'];
        return view('countriespackage.public.singleCountryDetails', $data);

    }

    public static function bookingAppointMent(Request $request)
    {
        $model  = new Countries();
        $model1 = new Countriespackage();
        $info   = $model1::makeInfo('countries');
        $data   = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );
        $data['countries'] = DB::table('ktmimmig_countries')->get();

        return view('bookappointmentdetails.public.form', $data);
    }

    public static function recentTestScoreDetails(Request $request)
    {
        $model  = new Countries();
        $model1 = new Countriespackage();
        $info   = $model1::makeInfo('countries');
        $data   = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );
        $id = $request->route('studentId');
//        $data['data'] = DB::table( 'ktmimmig_recent_test_score' )->where( 'id', $id )->get();;
        $data['data'] = DB::table('ktmimmig_recent_test_score')->get();

        return view('recenttestscore.public.index', $data);
    }

    public static function blog(Request $request)
    {
        $model = new Latestnewsnew();
        $info  = $model::makeInfo('Latestnewsnew');
        $data  = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );
        $newsId              = base64_decode($request->route('newsId'));
        $data['newsDetails'] = DB::table('latest_news')->where('id', $newsId)->get();
        $data['data']        = DB::table('latest_news')->paginate(6);

        return view('latestnewsnew.public.blog', $data);
    }

    public static function recentVisaSuccessDetails(Request $request)
    {
        $model = new recentvisasuccessnew();
        $info  = $model::makeInfo('recentvisasuccessnew');
        $data  = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );
        $id                            = $request->route('studentId');
        $data['data_students_details'] = DB::table('ktimmig_visa_success')->where('id', $id)->get();

        return view('recentvisasuccessnew.public.index', $data);
    }

    public static function visasuccesslist(Request $request)
    {
        $model = new recentvisasuccessnew();
        $info  = $model::makeInfo('recentvisasuccessnew');
        $data  = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );
        $data['data_all'] = DB::table('ktimmig_visa_success')->simplePaginate(15);

        return view('recentvisasuccessnew.public.listvisa', $data);
    }

    public static function getSignup(Request $request)
    {
        $model  = new Countries();
        $model1 = new Countriespackage();
        $info   = $model1::makeInfo('countries');
        $data   = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );
        $data['countries'] = DB::table('ktmimmig_countries')->get();

        return view('signUpdisplay', $data);
    }

    public static function getStudentSignupForm(Request $request)
    {
        $model  = new Countries();
        $model1 = new Countriespackage();
        $info   = $model1::makeInfo('countries');
        $data   = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );
        $data['qualificationLevel'] = DB::table('ktmimmig_qualification_level')->get();

        return view('studentdetails.public.form', $data);
    }

    public static function getIeltsBookinForm(Request $request)
    {
        $model = new Countries();
//        var_dump($model);
        $info = $model::makeInfo('countries');
        $data = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );

        return view('ieltsdatebookingclass.public.form', $data);
    }

    public static function getAgentSignupForm(Request $request)
    {
        $model  = new Countries();
        $model1 = new Countriespackage();
        $info   = $model1::makeInfo('countries');
        $data   = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );
        $data['qualificationLevel'] = DB::table('ktmimmig_qualification_level')->get();

        return view('registeredagents.public.form', $data);
    }

    public static function getSinglePackageDetails(Request $request)
    {
        $model1 = new Countriespackage();
        $info   = $model1::makeInfo('countries');
        $data   = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );
        $data['packageId'] = $packageId = base64_decode($request->route('packageId'));
        $data['countryId'] = $countryId = base64_decode($request->route('countryId'));
        $data['rowData']   = DB::table('ktmimmig_countries_package')->where('id', $packageId)->get();
        $data['rowData1']  = DB::table('ktmimmig_countries_package')->where('countryId', $countryId)->get();

        return view('countriespackage.public.singlePackageDetails', $data);

    }

    public static function getCountriesList()
    {
        $model = new Countries();
        $info  = $model::makeInfo('countries');
        $data  = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );

        $page   = isset($_GET['page']) ? $_GET['page'] : 1;
        $params = array(
            'page'   => $page,
            'limit'  => (isset($_GET['rows']) ? filter_var($_GET['rows'], FILTER_VALIDATE_INT) : 10),
            'sort'   => 'id',
            'order'  => 'asc',
            'params' => '',
            'global' => 1,
        );

        $result          = $model::getRows($params);
        $data['rowData'] = $result['rows'];

        return view('countries.public.countriesList', $data);
    }

    public function getStudentLoginForm()
    {
        $model = new Countries();
        $info  = $model::makeInfo('countries');
        $data  = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );

        return view('user.frontEndLogin', $data);
    }

    public static function universityList()
    {
        $model = new Countries();
        $info  = $model::makeInfo('countries');
        $data  = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',
        );
        $data['universityList'] = DB::table('universitylist')->orderBy('universityName', 'asc')->get();

        return view('universitylist.public.index', $data);
    }

    public static function singleUniDetails(Request $request)
    {
        $uniid = base64_decode($request->route('uniid'));
        $model = new Countries();
        $info  = $model::makeInfo('countries');
        $data  = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',
        );
        $data['data'] = DB::table('universitylist')->where('id', $uniid)->get();

        return view('universitylist.public.singleunidesc', $data);
    }

    public static function testpreparation(Request $request)
    {
        $model = new testpreparationlist();
        $info  = $model::makeInfo('testpreparationlist');
        $data  = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',
        );
        $row_id_cast                   = base64_decode($request->route('test_id'));
        $data['data_test_preparation'] = DB::table('test_preparation')->where('id', $row_id_cast)->get();
        // var_dump($data['data_test_preparation']);
        return view('testpreparationlist.public.single-details-test', $data);
    }

    public static function getApplyonlineForm()
    {
        $model = new Countries();
        $info  = $model::makeInfo('countries');
        $data  = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );
        $data['countriesList']      = DB::table('ktmimmig_countries')->get();
        $data['qualificationLevel'] = DB::table('ktmimmig_qualification_level')->get();

        return view('onlineapplicationsnewtwo.public.form', $data);
    }

    public static function bookIeltsClass()
    {
        $model = new Countries();
        $info  = $model::makeInfo('countries');
        $data  = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );
        $data['branchList'] = DB::table('ktmimmig_branches')->get();
        $data['intakeList'] = DB::table('ktmimmig_intake')->get();

        return view('ieltsclassbooking.public.form', $data);
    }

    public static function services_all(Request $request)
    {
        $model = new Countries();
        $info  = $model::makeInfo('countries');
        $data  = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );
        $serviceId              = base64_decode($request->route('serviceId'));
        $data['serviceDetails'] = DB::table('ktmimmig_services')->where('id', $serviceId)->get();
        $data['servicealllist'] = DB::table('ktmimmig_services')->get();

        return view('services.public.services', $data);
    }

    public static function getServices(Request $request)
    {
        $model = new Countries();
        $info  = $model::makeInfo('countries');
        $data  = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );
        $serviceId              = base64_decode($request->route('serviceId'));
        $data['serviceDetails'] = DB::table('ktmimmig_services')->where('id', $serviceId)->get();

        return view('services.public.index', $data);
    }

    public static function searchList(Request $request)
    {
        $model = new Countries();
        $info  = $model::makeInfo('countries');
        $data  = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',
        );

//        $this->validate($request, [
        //            'searchDestination' => 'required',
        //            'levelofcourse' => 'required',
        //            'areaofinterest' => 'required',
        //        ]);
        //

        $searchDestination         = $request->input('searchDestination');
        $levelofcourse             = $request->input('levelofcourse');
        $areaofinterest            = $request->input('areaofinterest');
        $data['searchListDetails'] = DB::select("select * from universitylist where universityCountry LIKE '%" . $searchDestination . "%'    OR universityCourseLevels LIKE '%" . $levelofcourse . "%' OR universityCourseTypes LIKE '%" . $areaofinterest . "%'");

        return view('layouts.default.template.search', $data);

    }

    public static function topsearchList(Request $request)
    {
        $model = new Countries();
        $info  = $model::makeInfo('countries');
        $data  = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',
        );
        $keyword = $request->input('keyword');

        var_dump($keyword);
//        SELECT * FROM course_types LEFT JOIN universitylist  ON course_types.id != universitylist.id
        $data['searchListDetails'] = DB::select("select * from universitylist where universityDescription LIKE '%" . $keyword . "%'	or universityCountry LIKE '%" . $keyword . "%' or universityCourseLevels LIKE '%" . $keyword . "%' or universityCourseTypes LIKE '%" . $keyword . "%'");
        return view('layouts.default.template.search', $data);
    }

    public static function getDownloads(Request $request)
    {
        $model = new Countries();
        $info  = $model::makeInfo('countries');
        $data  = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );
        $data['downloadsList'] = DB::table('ktmimmig_downloads')->get();

        return view('filedownloads.public.index', $data);
    }

    public static function dispPages(Request $request)
    {
        $model = new Countries();
        $info  = $model::makeInfo('countries');
//        var_dump($info);
        $data = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );
        $pageSlug            = $request->route('pageSlug');
        $data['pageDetails'] = DB::table('tb_pages')->where('alias', $pageSlug)->get();

        return view('cmspages.public.index', $data);
    }

    public static function viewNews(Request $request)
    {
        $model = new Latestnewsnew();
        $info  = $model::makeInfo('Latestnewsnew');
        $data  = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );
        $newsId              = base64_decode($request->route('newsId'));
        $data['newsDetails'] = DB::table('latest_news')->where('id', $newsId)->get();
        $data['newsListing'] = DB::table('latest_news')->take(3)->get();

//        var_dump($data);
        return view('latestnewsnew.public.singlenews', $data);
    }

    public static function getMockTestForm(Request $request)
    {
        $model  = new Countries();
        $model1 = new Countriespackage();
        $info   = $model1::makeInfo('countries');
        $data   = array(
            'pageTitle'    => $info['title'],
            'pageNote'     => $info['note'],
            'pageMetakey'  => 'Ktm Immigration, Study in Australia, Study in Usa, Study in Uk',
            'pageMetadesc' => 'Ktm Immigration is a popular abroad study consultancy service provider',

        );

        return view('mocktestbookingdetails.public.form', $data);
    }

    public function postProccess(Request $request, $formID)
    {
        //$row = $this->model->find($formID);
        $sql = \DB::table('tb_forms')->where('formID', $formID)->get();
        if (count($sql) <= 0) {
            return Redirect::back()->with('message', \SiteHelpers::alert('error', 'Form not Found !'));
        }

        $row           = $sql[0];
        $configuration = json_decode($row->configuration, true);

        $rules     = \FormHelpers::validateForm($configuration);
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $data = \FormHelpers::validatePost($request, $configuration);
            if ($row->method == 'table') {

                \DB::table($row->tablename)->insert($data);
                if ($row->redirect != '') {
                    echo '<script> window.location.href= "' . $row->redirect . '" </script>';
                } else {
                    return Redirect::back()->with('message', \SiteHelpers::alert('success', $row->success));
                }

            } else {
                // Send all input into specific email address

                $message = '';
                foreach ($configuration as $conf) {
                    $message .= '
						<b>' . $conf['label'] . '</b> : ' . $request->input($conf['field']) . ' <br />
					';
                }

                $data            = array('email' => $row->email, 'name' => $row->name);
                $data['message'] = $message;

                $message = view('core.forms.templateforms', $data);
                //Mail::send('core.forms.templateforms', $data, function ($message) use ($data) {

                //$message->to($data['email'])->subject('Submited Form :  '. $data['name']);
                //    });

                if ($row->redirect != '') {
                    echo '<script> window.location.href= "' . $row->redirect . '" </script>';
                } else {
                    return Redirect::back()->with('message', \SiteHelpers::alert('success', $row->success));
                }

            }

        } else {

            //Redirect::back();
            return Redirect::back()->with('message', \SiteHelpers::alert('error', 'The following errors occurred'))
                ->withErrors($validator)->withInput();

        }

    }

}
