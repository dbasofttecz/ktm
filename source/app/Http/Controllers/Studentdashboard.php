<?php 
namespace App\Http\Controllers;

use Mail;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Validator, Input, Redirect ;
use App\User;

/**
* 
*/
class Studentdashboard extends Controller
{
	
	public function dashboard(){
			return view('user.studentdash');
	}


	public function loginprocess(Request $request){
			  $rules = array(
            'email'    => 'required|email',
            'password' => 'required',
        );
        if (CNF_RECAPTCHA == 'true') {
            $rules['captcha'] = 'required|captcha';
        }

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {

            if (\Auth::attempt(array('email' => $request->input('email'), 'password' => $request->input('password')))) {
                if (\Auth::check()) {
                    $row = User::find(\Auth::user()->id);

                    if ($row->active == '0') {
                        // inactive
                        if ($request->ajax() == true) {
                            return response()->json(['status' => 'error', 'message' => 'Your Account is not active']);
                        } else {
                            \Auth::logout();
                            return Redirect::to('user/login')->with('message', \SiteHelpers::alert('error', 'Your Account is not active'));
                        }

                    } else if ($row->active == '2') {

                        if ($request->ajax() == true) {
                            return response()->json(['status' => 'error', 'message' => 'Your Account is BLocked']);
                        } else {
                            // BLocked users
                            \Auth::logout();
                            return Redirect::to('user/login')->with('message', \SiteHelpers::alert('error', 'Your Account is BLocked'));
                        }
                    } else {
                        \DB::table('tb_users')->where('id', '=', $row->id)->update(array('last_login' => date("Y-m-d H:i:s")));
                        \Session::put('uid', $row->id);
                        \Session::put('gid', $row->group_id);
                        \Session::put('eid', $row->email);
                        \Session::put('ll', $row->last_login);
                        \Session::put('fid', $row->first_name . ' ' . $row->last_name);
                        if (\Session::get('lang') == '') {
                            \Session::put('lang', CNF_LANG);
                        }

                        if ($request->ajax() == true) {
                            if (CNF_FRONT == 'false'):
                                return response()->json(['status' => 'success', 'url' => url('dashboard')]);
                            else:
                                return response()->json(['status' => 'success', 'url' => url('')]);
                            endif;

                        } else {
                            if (CNF_FRONT == 'true'):
                                return Redirect::to('student_dashboard');
                            else:
                                return Redirect::to('/');
                            endif;

                        }

                    }

                }

            } else {

                if ($request->ajax() == true) {
                    return response()->json(['status' => 'error', 'message' => 'Your username/password combination was incorrect']);
                } else {

                    return Redirect::to('studentLogin')
                        ->with('message', \SiteHelpers::alert('error', 'Your username/password combination was incorrect'))
                        ->withInput();
                }

            }
        } else {

            if ($request->ajax() == true) {
                return response()->json(['status' => 'error', 'message' => 'The following  errors occurred']);
            } else {

                return Redirect::to('studentLogin')
                    ->with('message', \SiteHelpers::alert('error', 'The following  errors occurred'))
                    ->withErrors($validator)->withInput();
            }

        }



    }

public function create(){
    var_dump("Create");
}

public function updates(){
    var_dump("updates");
}

}