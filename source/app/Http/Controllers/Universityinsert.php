<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Universityinserts;
use Input, Redirect;

class Universityinsert extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // var_dump("asdsd");

        return view('universityadmin.public.display');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
      //validator
      $this->validate($request,[
                       'univerisity_contry' => 'required',
                       'universityname' => 'required',
                       'location_univerisity' => 'required',
                       'establishdate' => 'required',
                       'no_of_students' => 'required',
                       'branches_university' => 'required',
                       'mytext' => 'required',
                       ]
       );


       $country_university = $request->input('univerisity_contry');
       $name_university = $request->input('universityname');
       $location_university = $request->input('location_univerisity');
       $establishment_university = $request->input('establishdate');
       $noofstudents_university = $request->input('no_of_students');
       $branches_university = $request->input('branches_university');
       $course_university = $request->input('mytext');
       foreach ($course_university as  $value_display_key) {
        $data_out = $value_display_key;
       }
      $data = array(
              "univerisity_contry" => $country_university,
              "universityname" => $name_university,
              "location_univerisity" => $location_university,
              "establishdate" => $establishment_university,
              "no_of_students" => $noofstudents_university,
              "branches_university" => $branches_university,
              "mytext" => $data_out,
 );
  DB::table('universitylistdetails')->insert($data);
  return redirect('univeristydashboard')->with('status', 'Successfully add records!');

}
 public function login(){
    return view('universityadmin.public.login');
 }

}
