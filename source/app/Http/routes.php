<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$active_multilang = defined( 'CNF_MULTILANG' ) ? CNF_LANG : 'en';
\App::setLocale( $active_multilang );
if ( defined( 'CNF_MULTILANG' ) && CNF_MULTILANG == '1' ) {

	$lang = ( \Session::get( 'lang' ) != "" ? \Session::get( 'lang' ) : CNF_LANG );
	\App::setLocale( $lang );
}

Route::get( '/', 'HomeController@index' );
Route::controller( 'home', 'HomeController' );
//countries route
Route::get( '/countriesList', 'HomeController@getCountriesList' );
Route::get( '/singleCountryDetails/{countryId}', 'HomeController@getSingleCountryDetails' );
Route::get( '/singlePackageDetails/{packageId}/{countryId}', 'HomeController@getSinglePackageDetails' );
Route::get( '/bookAppointment', 'HomeController@bookingAppointMent' );
Route::post( '/bookAppointmentPost', 'BookappointmentdetailsController@submitBookingAppointMentData' );
Route::get( '/studentSignup', 'HomeController@getStudentSignupForm' );
Route::get( '/agentSignup', 'HomeController@getAgentSignupForm' );
Route::post( '/studentSignupPost', 'UserController@submitRegistrationData' );
Route::post( '/agentSignupPost', 'UserController@submitAgentRegistrationData' );

// Route::post( '/studentLoginPost', 'UserController@userSignInPost' );
// Route::post( '/studentLoginPost', 'UserController@studentloginrequest' );


Route::controller( '/user', 'UserController' );
Route::get( '/applyOnlineNow', 'HomeController@getApplyonlineForm' );
Route::get( '/bookIeltsClass', 'HomeController@bookIeltsClass' );
Route::get( '/allservices', 'HomeController@services_all' );
Route::get( '/displayService/{serviceId}', 'HomeController@getServices' );
Route::get( '/downloads', 'HomeController@getDownloads' );
Route::get( '/viewNews/{newsId}', 'HomeController@viewNews' );
Route::get( '/dispPages/{pageSlug}', 'HomeController@dispPages' );
Route::get( '/universityList', 'HomeController@universityList' );
Route::get( '/bookMockTest', 'HomeController@getMockTestForm' );
Route::get( '/signUp', 'HomeController@getSignup' );
Route::get( '/ieltsBookingForm', 'HomeController@getIeltsBookinForm' );
Route::get( '/singleUniDetails/{uniid}', 'HomeController@singleUniDetails' );
Route::post( '/searchDetails', 'HomeController@searchList' );
//Route::post('/topSearchDetails', 'SearchController@search');
Route::post( '/topSearchDetails', 'HomeController@topsearchList' );
//Route::get( '/recentTestScoreDetails/{studentId}', 'HomeController@recentTestScoreDetails' );
Route::get( '/recentTestScoreDetails', 'HomeController@recentTestScoreDetails' );
Route::get( '/blog', 'HomeController@blog' );
Route::get( '/recentVisaSuccessDetails/{studentId}', 'HomeController@recentVisaSuccessDetails' );
Route::get( '/visasuccesslist', 'HomeController@visasuccesslist' );
Route::get( '/preparationdetails/{test_id}', 'HomeController@testpreparation' );

include( 'pageroutes.php' );
include( 'moduleroutes.php' );

Route::get( '/restric', function () {
	return view( 'errors.blocked' );
} );

Route::resource( 'sximoapi', 'SximoapiController' );
Route::group( [ 'middleware' => 'auth' ], function () {

	Route::get( 'core/elfinder', 'Core\ElfinderController@getIndex' );
	Route::post( 'core/elfinder', 'Core\ElfinderController@getIndex' );
	Route::controller( '/dashboard', 'DashboardController' );
	Route::controllers( [
		'core/users'    => 'Core\UsersController',
		'notification'  => 'NotificationController',
		'post'          => 'PostController',
		'core/logs'     => 'Core\LogsController',
		'core/pages'    => 'Core\PagesController',
		'core/groups'   => 'Core\GroupsController',
		'core/template' => 'Core\TemplateController',
		'core/posts'    => 'Core\PostsController',
		'core/forms'    => 'Core\FormsController',
	] );

} );

Route::group( [ 'middleware' => 'auth', 'middleware' => 'sximoauth' ], function () {

	Route::controllers( [
		'sximo/menu'   => 'Sximo\MenuController',
		'sximo/config' => 'Sximo\ConfigController',
		'sximo/module' => 'Sximo\ModuleController',
		'sximo/tables' => 'Sximo\TablesController',
		'sximo/code'   => 'Sximo\CodeController'
	] );


} );

/////// university details
//login
Route::get( '/univeristydashboard', 'Universityinsert@index' );
Route::post( '/univerisityentryprocess', 'Universityinsert@store' );
Route::get( '/universitylogin', 'Universityinsert@login' );
Route::post( '/universityloginprocess', 'universityloginprocess@index' );
#########################
Route::get( '/student_dashboard', 'Studentdashboard@dashboard' );
Route::get( '/studentLogin', 'HomeController@getStudentLoginForm' );
Route::post( '/studentloginprocess', 'Studentdashboard@loginprocess' );
Route::post( '/studentownupdateform', 'Studentdashboard@create' );
Route::post( '/studentupdateprocess', 'Studentdashboard@updates' );



