@extends('layouts.frontendMaster')
@section('contents')
        @foreach ($data_test_preparation as $display_single)
   
						<div class="header-section">
						<div class="container">
						    <div class="row">
						    <div class="col-md-12">
						    <h2>{{ $display_single->title_test }}</h2>
						    </div>
						</div>
						</div>
						</div>


	<div class="container-wrapper-single">
						<div class="container">
								<div class="row">
									<div class="col-md-12">
											<img src="{{ url('/uploads/testpreparation/'. $display_single->Image_test) }}" class="img-responsive align-left">
						  					<p>{{ $display_single->content_test }}</p>
						  		</div>
									</div>
								</div>
								</div>
					
				@endforeach
@endsection
