

		 {!! Form::open(array('url'=>'studentapplicants/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> studentApplicants</legend>
									
									  <div class="form-group  " >
										<label for="Id" class=" control-label col-md-4 text-left"> Id </label>
										<div class="col-md-7">
										  <input  type='text' name='id' id='id' value='{{ $row['id'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Name" class=" control-label col-md-4 text-left"> Applicant Name </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_name' id='applicant_name' value='{{ $row['applicant_name'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Gender" class=" control-label col-md-4 text-left"> Applicant Gender </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_gender' id='applicant_gender' value='{{ $row['applicant_gender'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Email" class=" control-label col-md-4 text-left"> Applicant Email </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_email' id='applicant_email' value='{{ $row['applicant_email'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Contact No" class=" control-label col-md-4 text-left"> Applicant Contact No </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_contact_no' id='applicant_contact_no' value='{{ $row['applicant_contact_no'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Qualification Level" class=" control-label col-md-4 text-left"> Applicant Qualification Level </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_qualification_level' id='applicant_qualification_level' value='{{ $row['applicant_qualification_level'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Intrested Country" class=" control-label col-md-4 text-left"> Applicant Intrested Country </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_intrested_country' id='applicant_intrested_country' value='{{ $row['applicant_intrested_country'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Ielts Marks" class=" control-label col-md-4 text-left"> Applicant Ielts Marks </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_ielts_marks' id='applicant_ielts_marks' value='{{ $row['applicant_ielts_marks'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Nat Marks" class=" control-label col-md-4 text-left"> Applicant Nat Marks </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_nat_marks' id='applicant_nat_marks' value='{{ $row['applicant_nat_marks'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Toefl Marks" class=" control-label col-md-4 text-left"> Applicant Toefl Marks </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_toefl_marks' id='applicant_toefl_marks' value='{{ $row['applicant_toefl_marks'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Sat Marks" class=" control-label col-md-4 text-left"> Applicant Sat Marks </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_sat_marks' id='applicant_sat_marks' value='{{ $row['applicant_sat_marks'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Gre Marks" class=" control-label col-md-4 text-left"> Applicant Gre Marks </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_gre_marks' id='applicant_gre_marks' value='{{ $row['applicant_gre_marks'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Gmat Marks" class=" control-label col-md-4 text-left"> Applicant Gmat Marks </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_gmat_marks' id='applicant_gmat_marks' value='{{ $row['applicant_gmat_marks'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Faculty" class=" control-label col-md-4 text-left"> Applicant Faculty </label>
										<div class="col-md-7">
										  <textarea name='applicant_faculty' rows='5' id='applicant_faculty' class='form-control '  
				           >{{ $row['applicant_faculty'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Occupation" class=" control-label col-md-4 text-left"> Applicant Occupation </label>
										<div class="col-md-7">
										  <textarea name='applicant_occupation' rows='5' id='applicant_occupation' class='form-control '  
				           >{{ $row['applicant_occupation'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Father Name" class=" control-label col-md-4 text-left"> Applicant Father Name </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_father_name' id='applicant_father_name' value='{{ $row['applicant_father_name'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Mother Name" class=" control-label col-md-4 text-left"> Applicant Mother Name </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_mother_name' id='applicant_mother_name' value='{{ $row['applicant_mother_name'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Marital Status" class=" control-label col-md-4 text-left"> Applicant Marital Status </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_marital_status' id='applicant_marital_status' value='{{ $row['applicant_marital_status'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Date Birth" class=" control-label col-md-4 text-left"> Applicant Date Birth </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_date_birth' id='applicant_date_birth' value='{{ $row['applicant_date_birth'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Citizenship No" class=" control-label col-md-4 text-left"> Applicant Citizenship No </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_citizenship_no' id='applicant_citizenship_no' value='{{ $row['applicant_citizenship_no'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Passport No" class=" control-label col-md-4 text-left"> Applicant Passport No </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_passport_no' id='applicant_passport_no' value='{{ $row['applicant_passport_no'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Passport Issue Date" class=" control-label col-md-4 text-left"> Applicant Passport Issue Date </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_passport_issue_date' id='applicant_passport_issue_date' value='{{ $row['applicant_passport_issue_date'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Passport Expiry Date" class=" control-label col-md-4 text-left"> Applicant Passport Expiry Date </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_passport_expiry_date' id='applicant_passport_expiry_date' value='{{ $row['applicant_passport_expiry_date'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Dependent Id" class=" control-label col-md-4 text-left"> Applicant Dependent Id </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_dependent_id' id='applicant_dependent_id' value='{{ $row['applicant_dependent_id'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
