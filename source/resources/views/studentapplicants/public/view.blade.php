<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Name</td>
						<td>{{ $row->applicant_name}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Gender</td>
						<td>{{ $row->applicant_gender}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Email</td>
						<td>{{ $row->applicant_email}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Contact No</td>
						<td>{{ $row->applicant_contact_no}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Qualification Level</td>
						<td>{{ $row->applicant_qualification_level}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Intrested Country</td>
						<td>{{ $row->applicant_intrested_country}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Ielts Marks</td>
						<td>{{ $row->applicant_ielts_marks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Nat Marks</td>
						<td>{{ $row->applicant_nat_marks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Toefl Marks</td>
						<td>{{ $row->applicant_toefl_marks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Sat Marks</td>
						<td>{{ $row->applicant_sat_marks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Gre Marks</td>
						<td>{{ $row->applicant_gre_marks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Gmat Marks</td>
						<td>{{ $row->applicant_gmat_marks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Faculty</td>
						<td>{{ $row->applicant_faculty}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Occupation</td>
						<td>{{ $row->applicant_occupation}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Father Name</td>
						<td>{{ $row->applicant_father_name}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Mother Name</td>
						<td>{{ $row->applicant_mother_name}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Marital Status</td>
						<td>{{ $row->applicant_marital_status}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Date Birth</td>
						<td>{{ $row->applicant_date_birth}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Citizenship No</td>
						<td>{{ $row->applicant_citizenship_no}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Passport No</td>
						<td>{{ $row->applicant_passport_no}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Passport Issue Date</td>
						<td>{{ $row->applicant_passport_issue_date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Passport Expiry Date</td>
						<td>{{ $row->applicant_passport_expiry_date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Dependent Id</td>
						<td>{{ $row->applicant_dependent_id}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	