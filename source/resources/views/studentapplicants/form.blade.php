@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		</div>
		<div class="sbox-tools " >
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 			
		</div> 

	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'studentapplicants/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset><legend> studentApplicants</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group  " >
										<label for="Applicant Name" class=" control-label col-md-4 text-left"> Applicant Name <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_name' id='applicant_name' value='{{ $row['applicant_name'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Gender" class=" control-label col-md-4 text-left"> Applicant Gender <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
					<label class='radio radio-inline'>
					<input type='radio' name='applicant_gender' value ='0' required @if($row['applicant_gender'] == '0') checked="checked" @endif > Male </label>
					<label class='radio radio-inline'>
					<input type='radio' name='applicant_gender' value ='1' required @if($row['applicant_gender'] == '1') checked="checked" @endif > Female </label> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Email" class=" control-label col-md-4 text-left"> Applicant Email <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_email' id='applicant_email' value='{{ $row['applicant_email'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Contact No" class=" control-label col-md-4 text-left"> Applicant Contact No <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_contact_no' id='applicant_contact_no' value='{{ $row['applicant_contact_no'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Qualification Level" class=" control-label col-md-4 text-left"> Applicant Qualification Level <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_qualification_level' id='applicant_qualification_level' value='{{ $row['applicant_qualification_level'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Intrested Country" class=" control-label col-md-4 text-left"> Applicant Intrested Country </label>
										<div class="col-md-7">
										  <select name='applicant_intrested_country' rows='5' id='applicant_intrested_country' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Ielts Marks" class=" control-label col-md-4 text-left"> Applicant Ielts Marks </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_ielts_marks' id='applicant_ielts_marks' value='{{ $row['applicant_ielts_marks'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Nat Marks" class=" control-label col-md-4 text-left"> Applicant Nat Marks </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_nat_marks' id='applicant_nat_marks' value='{{ $row['applicant_nat_marks'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Toefl Marks" class=" control-label col-md-4 text-left"> Applicant Toefl Marks </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_toefl_marks' id='applicant_toefl_marks' value='{{ $row['applicant_toefl_marks'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Sat Marks" class=" control-label col-md-4 text-left"> Applicant Sat Marks </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_sat_marks' id='applicant_sat_marks' value='{{ $row['applicant_sat_marks'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Gre Marks" class=" control-label col-md-4 text-left"> Applicant Gre Marks </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_gre_marks' id='applicant_gre_marks' value='{{ $row['applicant_gre_marks'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Gmat Marks" class=" control-label col-md-4 text-left"> Applicant Gmat Marks </label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_gmat_marks' id='applicant_gmat_marks' value='{{ $row['applicant_gmat_marks'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Faculty" class=" control-label col-md-4 text-left"> Applicant Faculty <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <textarea name='applicant_faculty' rows='5' id='applicant_faculty' class='form-control '  
				         required  >{{ $row['applicant_faculty'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Occupation" class=" control-label col-md-4 text-left"> Applicant Occupation </label>
										<div class="col-md-7">
										  <textarea name='applicant_occupation' rows='5' id='applicant_occupation' class='form-control '  
				           >{{ $row['applicant_occupation'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Father Name" class=" control-label col-md-4 text-left"> Applicant Father Name <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_father_name' id='applicant_father_name' value='{{ $row['applicant_father_name'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Mother Name" class=" control-label col-md-4 text-left"> Applicant Mother Name <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_mother_name' id='applicant_mother_name' value='{{ $row['applicant_mother_name'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Marital Status" class=" control-label col-md-4 text-left"> Applicant Marital Status <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
					<label class='radio radio-inline'>
					<input type='radio' name='applicant_marital_status' value ='0' required @if($row['applicant_marital_status'] == '0') checked="checked" @endif > Married </label>
					<label class='radio radio-inline'>
					<input type='radio' name='applicant_marital_status' value ='1' required @if($row['applicant_marital_status'] == '1') checked="checked" @endif > Unmarried </label> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Date Birth" class=" control-label col-md-4 text-left"> Applicant Date Birth <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('applicant_date_birth', $row['applicant_date_birth'],array('class'=>'form-control date','id'=>'applicant_date_birth')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Citizenship No" class=" control-label col-md-4 text-left"> Applicant Citizenship No <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_citizenship_no' id='applicant_citizenship_no' value='{{ $row['applicant_citizenship_no'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Passport No" class=" control-label col-md-4 text-left"> Applicant Passport No <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='applicant_passport_no' id='applicant_passport_no' value='{{ $row['applicant_passport_no'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Passport Issue Date" class=" control-label col-md-4 text-left"> Applicant Passport Issue Date <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('applicant_passport_issue_date', $row['applicant_passport_issue_date'],array('class'=>'form-control date','id'=>'applicant_passport_issue_date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Passport Expiry Date" class=" control-label col-md-4 text-left"> Applicant Passport Expiry Date <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('applicant_passport_expiry_date', $row['applicant_passport_expiry_date'],array('class'=>'form-control date','id'=>'applicant_passport_expiry_date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> {!! Form::hidden('applicant_dependent_id', $row['applicant_dependent_id']) !!}</fieldset>
			</div>
			
			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('studentapplicants?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#applicant_intrested_country").jCombo("{!! url('studentapplicants/comboselect?filter=tbl_countries:id:country_name') !!}",
		{  selected_value : '{{ $row["applicant_intrested_country"] }}' });
		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("studentapplicants/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop