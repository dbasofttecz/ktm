@extends('layouts.app')

@section('content')
<div class="page-content row">
 	<div class="page-content-wrapper m-t">   

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
  	<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><b>{{ $pageTitle }} : </b>  View Detail </a></li>
	@foreach($subgrid as $sub)
		<li role="presentation"><a href="#{{ str_replace(" ","_",$sub['title']) }}" aria-controls="profile" role="tab" data-toggle="tab"><b>{{ $pageTitle }}</b>  : {{ $sub['title'] }}</a></li>
	@endforeach
  </ul>

  <!-- Tab panes -->
  <div class="tab-content m-t">
  	<div role="tabpanel" class="tab-pane active" id="home">
  		
		<div class="sbox">
			<div class="sbox-title"> 
				<div class="sbox-tools pull-left" >
			   		<a href="{{ url('studentapplicants?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
					@if($access['is_add'] ==1)
			   		<a href="{{ url('studentapplicants/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
					@endif 
							
				</div>	

				<div class="sbox-tools " >
					<a href="{{ ($prevnext['prev'] != '' ? url('studentapplicants/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
					<a href="{{ ($prevnext['next'] != '' ? url('studentapplicants/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
					@if(Session::get('gid') ==1)
						<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
					@endif 			
				</div> 
			</div>
			<div class="sbox-content" > 	

				<table class="table table-striped table-bordered" >
					<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Name</td>
						<td>{{ $row->applicant_name}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Gender</td>
						<td>{{ $row->applicant_gender}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Email</td>
						<td>{{ $row->applicant_email}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Contact No</td>
						<td>{{ $row->applicant_contact_no}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Qualification Level</td>
						<td>{{ $row->applicant_qualification_level}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Intrested Country</td>
						<td>{{ $row->applicant_intrested_country}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Ielts Marks</td>
						<td>{{ $row->applicant_ielts_marks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Nat Marks</td>
						<td>{{ $row->applicant_nat_marks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Toefl Marks</td>
						<td>{{ $row->applicant_toefl_marks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Sat Marks</td>
						<td>{{ $row->applicant_sat_marks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Gre Marks</td>
						<td>{{ $row->applicant_gre_marks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Gmat Marks</td>
						<td>{{ $row->applicant_gmat_marks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Faculty</td>
						<td>{{ $row->applicant_faculty}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Occupation</td>
						<td>{{ $row->applicant_occupation}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Father Name</td>
						<td>{{ $row->applicant_father_name}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Mother Name</td>
						<td>{{ $row->applicant_mother_name}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Marital Status</td>
						<td>{{ $row->applicant_marital_status}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Date Birth</td>
						<td>{{ $row->applicant_date_birth}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Citizenship No</td>
						<td>{{ $row->applicant_citizenship_no}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Passport No</td>
						<td>{{ $row->applicant_passport_no}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Passport Issue Date</td>
						<td>{{ $row->applicant_passport_issue_date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Passport Expiry Date</td>
						<td>{{ $row->applicant_passport_expiry_date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Dependent Id</td>
						<td>{{ $row->applicant_dependent_id}} </td>
						
					</tr>
				
						
					</tbody>	
				</table>   
			
			</div>
		</div>	
  	</div>
  	@foreach($subgrid as $sub)
  		<div role="tabpanel" class="tab-pane" id="{{ str_replace(" ","_",$sub['title']) }}"></div>
  	@endforeach
  </div>


</div>
</div>


<script type="text/javascript">
	$(function(){
		<?php for($i=0 ; $i<count($subgrid); $i++)  :?>
			$('#{{ str_replace(" ","_",$subgrid[$i]['title']) }}').load('{{ url("studentapplicants/lookup/".implode("-",$subgrid["$i"])."-".$id)}}')
		<?php endfor;?>
	})

</script>
	  
@stop