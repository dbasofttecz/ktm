

		 {!! Form::open(array('url'=>'latestnewsnew/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Latest News</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group  " >
										<label for="NewsTitle" class=" control-label col-md-4 text-left"> NewsTitle <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='newsTitle' id='newsTitle' value='{{ $row['newsTitle'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="NewsDescription" class=" control-label col-md-4 text-left"> NewsDescription <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <textarea name='newsDescription' rows='5' id='editor' class='form-control editor '  
						required >{{ $row['newsDescription'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Country" class=" control-label col-md-4 text-left"> Country <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <select name='country' rows='5' id='country' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="FeaturedImage" class=" control-label col-md-4 text-left"> FeaturedImage </label>
										<div class="col-md-7">
										  <input  type='file' name='featuredImage' id='featuredImage' @if($row['featuredImage'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['featuredImage'],'/uploads/latestNews/') !!}
						
						</div>					
					 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#country").jCombo("{!! url('latestnewsnew/comboselect?filter=ktmimmig_countries:id:countryName') !!}",
		{  selected_value : '{{ $row["country"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
