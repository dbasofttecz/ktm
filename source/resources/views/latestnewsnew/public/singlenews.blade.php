@extends('layouts.frontendMaster')
@section('contents')
    @foreach($newsDetails as $singlenews)
        <div class="header-section-single" style="background-image: url({{'../frontend/default/img/news.jpeg'}})">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="header-single-news-details">
                            <h1>{{$singlenews->newsTitle}}</h1>
                            <time></time>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="details-news">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="content-wrapper-single">
                            <div class="img-src"
                                 style="background-image: url({{'../uploads/latestNews/'.$singlenews->featuredImage}})"></div>
                            <p>{!! $singlenews->newsDescription !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach


    <!--related post-->
    <div class="related-posted">
        <div class="container">
            <div class="row row-centered">
                <h1>Related News</h1>
                @foreach($newsListing as $all_post_news)
                    <div class="col-md-4 col-centered">
                        <div class="img-res"
                             style="background-image: url({{'../uploads/latestNews/'.$all_post_news->featuredImage}})"></div>
                        <div class="content-related">
                            <h2>{{$all_post_news->newsTitle}}</h2>
                            <p>{!! strip_tags(substr($all_post_news->newsDescription,0,100)) !!}</p>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection