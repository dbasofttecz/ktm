@extends('layouts.frontendMaster')
@section('contents')
    <div class="container">
        <div class="row">

            @foreach($data as $display_all)
                <div class="col-md-4">
                    <div class="list-details">
                    <?php 
                    $images_original =  asset('uploads/latestNews/'.$display_all->featuredImage);    
                    ?>
                    <div class="images-responsive-blog-display" style="background-image: url(<?php echo $images_original; ?>)"></div>
                        <div class="content-wrapper">
                            <a href="{{ url('/viewNews/'.base64_encode($display_all->id)) }}">
                                <h2><?php echo $display_all->newsTitle; ?></h2>
                            </a>
                            <p>{{ substr(strip_tags($display_all->newsDescription), 0, 100) }}</p>
                        </div>
                    </div>
                </div>

            @endforeach
            

        </div>
        <div class="pagiation-custom-laravel text-center"> 
         {!! $data->render() !!}
         </div>
    </div>
@endsection