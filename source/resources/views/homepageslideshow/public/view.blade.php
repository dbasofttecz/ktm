<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Slide Name</td>
						<td>{{ $row->slideName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Slide Description</td>
						<td>{{ $row->slideDescription}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Slide Link</td>
						<td><a href="">{{ $row->slideLink}} </a> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Slide Image</td>
						<td>{!! SiteHelpers::formatRows($row->slideImage,$fields['slideImage'],$row ) !!} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	