<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Country Name</td>
						<td>{{ $row->countryName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Country Description</td>
						<td>{{ $row->countryDesc}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Featured Image</td>
						<td>{!! SiteHelpers::formatRows($row->featuredImage,$fields['featuredImage'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Banner Image</td>
						<td>{!! SiteHelpers::formatRows($row->bannerImage,$fields['bannerImage'],$row ) !!} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	