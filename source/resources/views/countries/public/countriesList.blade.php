@extends('layouts.frontendMaster')
@section('contents')
    <!-- <div class="bg-image page-title">
	<div class="container-fluid">
		<a href="#"><h1>COUNTRIES</h1></a>
		<div class="pull-right">
			<a href="01_home.html"><i class="fa fa-home fa-lg"></i></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="05_services.html">Our services</a>
		</div>
	</div>
</div> -->

    <div class="container block-content">
        <div class="row main-grid">
            <div class="col-sm-12">
                <div class="row services">
                    @foreach($rowData as $row)
                        <div class="service-item col-xs-6 col-sm-4 col-md-4 col-lg-4 wow zoomIn" data-wow-delay="0.3s">
                            <img class="full-width" src="{{url('/uploads/countries/'.$row->featuredImage)}}" alt="Img">
                            <h4>{{$row->countryName}}</h4>
                            <p class="countrySummary">{{ substr(strip_tags($row->countryDesc),0, 200) }}</p>
                            <a class="btn btn-default btn-sm"
                               href="{{ url('/singleCountryDetails/'.base64_encode($row->id)) }}">READ MORE</a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection