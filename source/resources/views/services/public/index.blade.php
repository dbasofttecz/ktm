@extends('layouts.frontendMaster')
@section('contents')
<div class="container" style="padding-top:25px;">
	@foreach($serviceDetails as $row)	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{$row->serviceName}} </h3>
        <hr />       
    </div>
    @endforeach
</div>
<div class="container m-t">		
 	@foreach($serviceDetails as $row)
 		{{$row->serviceDescription}}
 	@endforeach
</div>
@endsection