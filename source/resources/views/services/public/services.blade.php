@extends('layouts.frontendMaster')
@section('contents')
    <div class="container">
        <div class="row column-info">
            @foreach($servicealllist as $list_all)
                <div class="col-md-12 wrap-container-list fadeInLeft" data-wow-delay="0.3s"
                     style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
                    <div class="container-list-item">
                        {{--<img src="{{url('/uploads/slideshow/'.$list_all->slideImage)}}" alt="Img">--}}
                        {{--<span></span>--}}
                        <h3>{!! $list_all->serviceName !!}</h3>
                        <p class="countrySummary">{!! substr(strip_tags($list_all->serviceDescription),0,400) !!}</p>
                    </div>
                    <div class="btnbtn-warning">
                        <a class="btn btn-default btn-sm"
                           href="{{url('displayService/'.base64_encode($list_all->id))}}">READ MORE</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection