@extends('layouts.frontendMaster')
@section('contents')
<div class="container">
{!! Form::open(array('url'=>'mocktestbookingdetails/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
@if(Session::has('messagetext'))
{!! Session::get('messagetext') !!}
@endif
<ul class="parsley-error-list">
   @foreach($errors->all() as $error)
   <li>{{ $error }}</li>
   @endforeach
</ul>
<div class="col-md-12">
   <fieldset>
      <legend> Mock Test Booking Details</legend>
      <div class="form-group  " style="display:none" >
         <label for="Id" class=" control-label col-md-4 text-left"> Id </label>
         <div class="col-md-7">
            <input  type='text' name='id' id='id' 
            class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Applicant Name" class=" control-label col-md-4 text-left"> Applicant Name <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='text' name='applicantName' id='applicantName' 
            required     class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Applicant Gender" class=" control-label col-md-4 text-left"> Applicant Gender <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <label class='radio radio-inline'>
            <input type='radio' name='applicantGender' value ='M' required  > Male </label>
            <label class='radio radio-inline'>
            <input type='radio' name='applicantGender' value ='F' required  > Female </label> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Applicant Contact No" class=" control-label col-md-4 text-left"> Applicant Contact No <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='text' name='applicantContactNo' id='applicantContactNo' 
            required     class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Applicant Email Address" class=" control-label col-md-4 text-left"> Applicant Email Address <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='text' name='applicantEmailAddress' id='applicantEmailAddress'  
            required     class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Applicant Interested Date" class=" control-label col-md-4 text-left"> Applicant Interested Date <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <div class="input-group m-b" style="width:150px !important;">
               {!! Form::text('applicantInterestedDate', '',array('class'=>'form-control date', 'style'=>'width:150px !important;')) !!}
               <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Applicant Interested Time" class=" control-label col-md-4 text-left"> Applicant Interested Time <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <div class="input-group m-b" style="width:150px !important;">
               {!! Form::text('applicantInterestedTime', '',array('class'=>'form-control time', 'style'=>'width:150px !important;')) !!}
               <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
         </div>
         <div class="col-md-1">
         </div>
      </div>
   </fieldset>
</div>
<div style="clear:both"></div>
<div class="form-group">
   <label class="col-sm-4 text-right">&nbsp;</label>
   <div class="col-sm-8">	

      <button type="submit" name="submit" class="btn btn-default btn-sm" ><i class="fa  fa-save "></i> Book Now</button>
   </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
   $(document).ready(function() {    	
         	//custom date 
      $('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
      $('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
   
   	$('.removeCurrentFiles').on('click',function(){
   		var removeUrl = $(this).attr('href');
   		$.get(removeUrl,function(response){});
   		$(this).parent('div').empty();	
   		return false;
   	});		
   	
   });
</script>
</div>	
@endsection