<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Name</td>
						<td>{{ $row->applicantName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Gender</td>
						<td>{{ $row->applicantGender}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Contact No</td>
						<td>{{ $row->applicantContactNo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Email Address</td>
						<td>{{ $row->applicantEmailAddress}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Interested Date</td>
						<td>{{ $row->applicantInterestedDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Interested Time</td>
						<td>{{ $row->applicantInterestedTime}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	