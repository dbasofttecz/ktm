<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Module Id</td>
						<td>{{ $row->module_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Module Name</td>
						<td>{{ $row->module_name}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Module Title</td>
						<td>{{ $row->module_title}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Module Note</td>
						<td>{{ $row->module_note}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Module Author</td>
						<td>{{ $row->module_author}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Module Created</td>
						<td>{{ $row->module_created}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Module Desc</td>
						<td>{{ $row->module_desc}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Module Db</td>
						<td>{{ $row->module_db}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Module Db Key</td>
						<td>{{ $row->module_db_key}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Module Type</td>
						<td>{{ $row->module_type}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Module Config</td>
						<td>{{ $row->module_config}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Module Lang</td>
						<td>{{ $row->module_lang}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	