

		 {!! Form::open(array('url'=>'examplemodule/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> exampleModule</legend>
									
									  <div class="form-group  " >
										<label for="Module Id" class=" control-label col-md-4 text-left"> Module Id </label>
										<div class="col-md-7">
										  <input  type='text' name='module_id' id='module_id' value='{{ $row['module_id'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Module Name" class=" control-label col-md-4 text-left"> Module Name </label>
										<div class="col-md-7">
										  <input  type='text' name='module_name' id='module_name' value='{{ $row['module_name'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Module Title" class=" control-label col-md-4 text-left"> Module Title </label>
										<div class="col-md-7">
										  <input  type='text' name='module_title' id='module_title' value='{{ $row['module_title'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Module Note" class=" control-label col-md-4 text-left"> Module Note </label>
										<div class="col-md-7">
										  <input  type='text' name='module_note' id='module_note' value='{{ $row['module_note'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Module Author" class=" control-label col-md-4 text-left"> Module Author </label>
										<div class="col-md-7">
										  <input  type='text' name='module_author' id='module_author' value='{{ $row['module_author'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Module Created" class=" control-label col-md-4 text-left"> Module Created </label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('module_created', $row['module_created'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Module Desc" class=" control-label col-md-4 text-left"> Module Desc </label>
										<div class="col-md-7">
										  <textarea name='module_desc' rows='5' id='module_desc' class='form-control '  
				           >{{ $row['module_desc'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Module Db" class=" control-label col-md-4 text-left"> Module Db </label>
										<div class="col-md-7">
										  <input  type='text' name='module_db' id='module_db' value='{{ $row['module_db'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Module Db Key" class=" control-label col-md-4 text-left"> Module Db Key </label>
										<div class="col-md-7">
										  <input  type='text' name='module_db_key' id='module_db_key' value='{{ $row['module_db_key'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Module Type" class=" control-label col-md-4 text-left"> Module Type </label>
										<div class="col-md-7">
										  <input  type='text' name='module_type' id='module_type' value='{{ $row['module_type'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Module Config" class=" control-label col-md-4 text-left"> Module Config </label>
										<div class="col-md-7">
										  <input  type='text' name='module_config' id='module_config' value='{{ $row['module_config'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Module Lang" class=" control-label col-md-4 text-left"> Module Lang </label>
										<div class="col-md-7">
										  <textarea name='module_lang' rows='5' id='module_lang' class='form-control '  
				           >{{ $row['module_lang'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
