@extends('layouts.frontendMaster')
@section('contents')
<div class="container-fluid block-content">
	<div class="row main-grid">
		<div class="col-sm-12">
			<div class="row services">
			   <div class="col-md-12 text-center">
			   <p>If you are a student applying abroad, please create your account here.</p>
			   	 <a href="{{url('studentSignup')}}" class="btn btn-success">Signup</a>
			   </div>
			   {{--<div class="col-md-6 text-center">--}}
			   	{{--<p>If you are a agent, please create your account here.</p>	--}}
			   	 {{--<a href="{{url('agentSignup')}}" class="btn btn-success">Signup As Agent</a>--}}
			   {{--</div>--}}
			</div>
		</div>
	</div>            
</div>
@endsection