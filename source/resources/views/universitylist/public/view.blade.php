<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>University Name</td>
						<td>{{ $row->universityName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>University Logo</td>
						<td>{{ $row->universityLogo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>University Description</td>
						<td>{{ $row->universityDescription}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>University Country</td>
						<td>{{ $row->universityCountry}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>University Course Levels</td>
						<td>{{ $row->universityCourseLevels}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>University Course Types</td>
						<td>{{ $row->universityCourseTypes}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	