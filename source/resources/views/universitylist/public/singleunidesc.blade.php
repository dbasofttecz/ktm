@extends('layouts.frontendMaster')
@section('contents')

    <div class="container">
        <div class="row">
            <div style="padding-top:25px;">
                @foreach($data as $row)
                    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
                        <h3> {{$row->universityName}} </h3>
                        <hr/>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="m-t">
                @foreach($data as $row)
                    {!! $row->universityDescription !!}
                @endforeach
            </div>
        </div>
    </div>
@endsection