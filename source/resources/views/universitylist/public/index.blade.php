@extends('layouts.frontendMaster')
@section('contents')
<div class="container block-content">
	<div class="row main-grid">
		<div class="col-sm-12">
			<div class="row services">
			    @foreach($universityList as $row)
				<div class="service-item col-xs-6 col-sm-4 col-md-4 col-lg-4 wow zoomIn" data-wow-delay="0.3s">
					<img class="full-width" src="{{url('/uploads/unilogo/'.$row->universityLogo)}}" alt="Img">
					<h4>{{$row->universityName}}</h4>
					<p class="countrySummary">{!! str_limit(strip_tags($row->universityDescription),200) !!}</p>
					<a class="btn btn-default btn-sm" href="{{ url('/singleUniDetails/'.base64_encode($row->id)) }}">READ MORE</a>
				</div>
				@endforeach
			</div>
		</div>
	</div>            
</div>
@endsection