@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		</div>
		<div class="sbox-tools " >
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 			
		</div> 

	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'universitylist/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset><legend> University List</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group  " >
										<label for="UniversityName" class=" control-label col-md-4 text-left"> UniversityName <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='universityName' id='universityName' value='{{ $row['universityName'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="UniversityLogo" class=" control-label col-md-4 text-left"> UniversityLogo </label>
										<div class="col-md-7">
										  <input  type='file' name='universityLogo' id='universityLogo' @if($row['universityLogo'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['universityLogo'],'/uploads/unilogo/') !!}
						
						</div>					
					 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="UniversityDescription" class=" control-label col-md-4 text-left"> UniversityDescription <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <textarea name='universityDescription' rows='5' id='editor' class='form-control editor '  
						required >{{ $row['universityDescription'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="UniversityCountry" class=" control-label col-md-4 text-left"> UniversityCountry <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <select name='universityCountry' rows='5' id='universityCountry' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="UniversityCourseLevels" class=" control-label col-md-4 text-left"> UniversityCourseLevels <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <select name='universityCourseLevels[]' multiple rows='5' id='universityCourseLevels' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="UniversityCourseTypes" class=" control-label col-md-4 text-left"> UniversityCourseTypes <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <select name='universityCourseTypes[]' multiple rows='5' id='universityCourseTypes' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('universitylist?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#universityCountry").jCombo("{!! url('universitylist/comboselect?filter=ktmimmig_countries:countryName:countryName') !!}",
		{  selected_value : '{{ $row["universityCountry"] }}' });
		
		$("#universityCourseLevels").jCombo("{!! url('universitylist/comboselect?filter=study_level:studyLevelName:studyLevelName') !!}",
		{  selected_value : '{{ $row["universityCourseLevels"] }}' });
		
		$("#universityCourseTypes").jCombo("{!! url('universitylist/comboselect?filter=course_types:course_level_name:course_level_name') !!}",
		{  selected_value : '{{ $row["universityCourseTypes"] }}' });
		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("universitylist/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop