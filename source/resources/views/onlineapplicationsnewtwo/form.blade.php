@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		</div>
		<div class="sbox-tools " >
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 			
		</div> 

	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'onlineapplicationsnewtwo/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<ul class="nav nav-tabs"><li class="active"><a href="#OnlineApplicationsStudent" data-toggle="tab">Online Applications Student</a></li>
				<li class=""><a href="#OnlineApplicationsDependant" data-toggle="tab">Online Applications Dependant</a></li>
				</ul><div class="tab-content"><div class="tab-pane m-t active" id="OnlineApplicationsStudent"> 
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group  " >
										<label for="Applicant Name" class=" control-label col-md-4 text-left"> Applicant Name <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='applicantName' id='applicantName' value='{{ $row['applicantName'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Gender" class=" control-label col-md-4 text-left"> Applicant Gender <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
					<label class='radio radio-inline'>
					<input type='radio' name='applicantGender' value ='M' required @if($row['applicantGender'] == 'M') checked="checked" @endif > Male </label>
					<label class='radio radio-inline'>
					<input type='radio' name='applicantGender' value ='F' required @if($row['applicantGender'] == 'F') checked="checked" @endif > Female </label> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Email" class=" control-label col-md-4 text-left"> Applicant Email <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='applicantEmail' id='applicantEmail' value='{{ $row['applicantEmail'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Contact No" class=" control-label col-md-4 text-left"> Applicant Contact No <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='applicantContactNo' id='applicantContactNo' value='{{ $row['applicantContactNo'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Qualification Level" class=" control-label col-md-4 text-left"> Applicant Qualification Level <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <select name='applicantQualificationLevel' rows='5' id='applicantQualificationLevel' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Interested Country" class=" control-label col-md-4 text-left"> Applicant Interested Country <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <select name='applicantInterestedCountry' rows='5' id='applicantInterestedCountry' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Ielts Marks" class=" control-label col-md-4 text-left"> Applicant Ielts Marks </label>
										<div class="col-md-7">
										  <input  type='text' name='applicantIeltsMarks' id='applicantIeltsMarks' value='{{ $row['applicantIeltsMarks'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Nat Marks" class=" control-label col-md-4 text-left"> Applicant Nat Marks </label>
										<div class="col-md-7">
										  <input  type='text' name='applicantNatMarks' id='applicantNatMarks' value='{{ $row['applicantNatMarks'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Sat Marks" class=" control-label col-md-4 text-left"> Applicant Sat Marks </label>
										<div class="col-md-7">
										  <input  type='text' name='applicantSatMarks' id='applicantSatMarks' value='{{ $row['applicantSatMarks'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Gre Marks" class=" control-label col-md-4 text-left"> Applicant Gre Marks </label>
										<div class="col-md-7">
										  <input  type='text' name='applicantGreMarks' id='applicantGreMarks' value='{{ $row['applicantGreMarks'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Gmat Marks" class=" control-label col-md-4 text-left"> Applicant Gmat Marks </label>
										<div class="col-md-7">
										  <input  type='text' name='applicantGmatMarks' id='applicantGmatMarks' value='{{ $row['applicantGmatMarks'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Faculty" class=" control-label col-md-4 text-left"> Applicant Faculty <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='applicantFaculty' id='applicantFaculty' value='{{ $row['applicantFaculty'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Occupation" class=" control-label col-md-4 text-left"> Applicant Occupation <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='applicantOccupation' id='applicantOccupation' value='{{ $row['applicantOccupation'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Father Name" class=" control-label col-md-4 text-left"> Applicant Father Name <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='applicantFatherName' id='applicantFatherName' value='{{ $row['applicantFatherName'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicant Mother Name" class=" control-label col-md-4 text-left"> Applicant Mother Name <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='applicantMotherName' id='applicantMotherName' value='{{ $row['applicantMotherName'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="MartialStatus" class=" control-label col-md-4 text-left"> MartialStatus <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
					<label class='radio radio-inline'>
					<input type='radio' name='martialStatus' value ='married' required @if($row['martialStatus'] == 'married') checked="checked" @endif > Married </label>
					<label class='radio radio-inline'>
					<input type='radio' name='martialStatus' value ='single' required @if($row['martialStatus'] == 'single') checked="checked" @endif > Single </label> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Language Certified Documents" class=" control-label col-md-4 text-left"> Language Certified Documents <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
					<a href="javascript:void(0)" class="btn btn-xs btn-primary pull-right" onclick="addMoreFiles('language_certified_documents')"><i class="fa fa-plus"></i></a>
					<div class="language_certified_documentsUpl">	
					 	<input  type='file' name='language_certified_documents[]'  />			
					</div>
					<ul class="uploadedLists " >
					<?php $cr= 0; 
					$row['language_certified_documents'] = explode(",",$row['language_certified_documents']);
					?>
					@foreach($row['language_certified_documents'] as $files)
						@if(file_exists('./uploads/applyonlinenew/language/'.$files) && $files !='')
						<li id="cr-<?php echo $cr;?>" class="">							
							<a href="{{ url('/uploads/applyonlinenew/language//'.$files) }}" target="_blank" >{{ $files }}</a> 
							<span class="pull-right removeMultiFiles" rel="cr-<?php echo $cr;?>" url="/uploads/applyonlinenew/language/{{$files}}">
							<i class="fa fa-trash-o  btn btn-xs btn-danger"></i></span>
							<input type="hidden" name="currlanguage_certified_documents[]" value="{{ $files }}"/>
							<?php ++$cr;?>
						</li>
						@endif
					
					@endforeach
					</ul>
					 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Academic Documents" class=" control-label col-md-4 text-left"> Academic Documents <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
					<a href="javascript:void(0)" class="btn btn-xs btn-primary pull-right" onclick="addMoreFiles('academic_documents')"><i class="fa fa-plus"></i></a>
					<div class="academic_documentsUpl">	
					 	<input  type='file' name='academic_documents[]'  />			
					</div>
					<ul class="uploadedLists " >
					<?php $cr= 0; 
					$row['academic_documents'] = explode(",",$row['academic_documents']);
					?>
					@foreach($row['academic_documents'] as $files)
						@if(file_exists('./uploads/applyonlinenew/academics/'.$files) && $files !='')
						<li id="cr-<?php echo $cr;?>" class="">							
							<a href="{{ url('/uploads/applyonlinenew/academics//'.$files) }}" target="_blank" >{{ $files }}</a> 
							<span class="pull-right removeMultiFiles" rel="cr-<?php echo $cr;?>" url="/uploads/applyonlinenew/academics/{{$files}}">
							<i class="fa fa-trash-o  btn btn-xs btn-danger"></i></span>
							<input type="hidden" name="curracademic_documents[]" value="{{ $files }}"/>
							<?php ++$cr;?>
						</li>
						@endif
					
					@endforeach
					</ul>
					 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Financial Documents" class=" control-label col-md-4 text-left"> Financial Documents <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
					<a href="javascript:void(0)" class="btn btn-xs btn-primary pull-right" onclick="addMoreFiles('financial_documents')"><i class="fa fa-plus"></i></a>
					<div class="financial_documentsUpl">	
					 	<input  type='file' name='financial_documents[]'  />			
					</div>
					<ul class="uploadedLists " >
					<?php $cr= 0; 
					$row['financial_documents'] = explode(",",$row['financial_documents']);
					?>
					@foreach($row['financial_documents'] as $files)
						@if(file_exists('./uploads/applyonlinenew/financial/'.$files) && $files !='')
						<li id="cr-<?php echo $cr;?>" class="">							
							<a href="{{ url('/uploads/applyonlinenew/financial//'.$files) }}" target="_blank" >{{ $files }}</a> 
							<span class="pull-right removeMultiFiles" rel="cr-<?php echo $cr;?>" url="/uploads/applyonlinenew/financial/{{$files}}">
							<i class="fa fa-trash-o  btn btn-xs btn-danger"></i></span>
							<input type="hidden" name="currfinancial_documents[]" value="{{ $files }}"/>
							<?php ++$cr;?>
						</li>
						@endif
					
					@endforeach
					</ul>
					 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Work Experience Documents" class=" control-label col-md-4 text-left"> Work Experience Documents </label>
										<div class="col-md-7">
										  
					<a href="javascript:void(0)" class="btn btn-xs btn-primary pull-right" onclick="addMoreFiles('work_experience_documents')"><i class="fa fa-plus"></i></a>
					<div class="work_experience_documentsUpl">	
					 	<input  type='file' name='work_experience_documents[]'  />			
					</div>
					<ul class="uploadedLists " >
					<?php $cr= 0; 
					$row['work_experience_documents'] = explode(",",$row['work_experience_documents']);
					?>
					@foreach($row['work_experience_documents'] as $files)
						@if(file_exists('./uploads/applyonlinenew/workexperience/'.$files) && $files !='')
						<li id="cr-<?php echo $cr;?>" class="">							
							<a href="{{ url('/uploads/applyonlinenew/workexperience//'.$files) }}" target="_blank" >{{ $files }}</a> 
							<span class="pull-right removeMultiFiles" rel="cr-<?php echo $cr;?>" url="/uploads/applyonlinenew/workexperience/{{$files}}">
							<i class="fa fa-trash-o  btn btn-xs btn-danger"></i></span>
							<input type="hidden" name="currwork_experience_documents[]" value="{{ $files }}"/>
							<?php ++$cr;?>
						</li>
						@endif
					
					@endforeach
					</ul>
					 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Additional Documents" class=" control-label col-md-4 text-left"> Additional Documents </label>
										<div class="col-md-7">
										  
					<a href="javascript:void(0)" class="btn btn-xs btn-primary pull-right" onclick="addMoreFiles('additional_documents')"><i class="fa fa-plus"></i></a>
					<div class="additional_documentsUpl">	
					 	<input  type='file' name='additional_documents[]'  />			
					</div>
					<ul class="uploadedLists " >
					<?php $cr= 0; 
					$row['additional_documents'] = explode(",",$row['additional_documents']);
					?>
					@foreach($row['additional_documents'] as $files)
						@if(file_exists('./uploads/applyonlinenew/additionaldocuments/'.$files) && $files !='')
						<li id="cr-<?php echo $cr;?>" class="">							
							<a href="{{ url('/uploads/applyonlinenew/additionaldocuments//'.$files) }}" target="_blank" >{{ $files }}</a> 
							<span class="pull-right removeMultiFiles" rel="cr-<?php echo $cr;?>" url="/uploads/applyonlinenew/additionaldocuments/{{$files}}">
							<i class="fa fa-trash-o  btn btn-xs btn-danger"></i></span>
							<input type="hidden" name="curradditional_documents[]" value="{{ $files }}"/>
							<?php ++$cr;?>
						</li>
						@endif
					
					@endforeach
					</ul>
					 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Process Status" class=" control-label col-md-4 text-left"> Process Status </label>
										<div class="col-md-7">
										  <input  type='text' name='process_status' id='process_status' value='{{ $row['process_status'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Process Remarks" class=" control-label col-md-4 text-left"> Process Remarks </label>
										<div class="col-md-7">
										  <textarea name='process_remarks' rows='5' id='process_remarks' class='form-control '  
				           >{{ $row['process_remarks'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 
			</div>
			
			<div class="tab-pane m-t " id="OnlineApplicationsDependant"> 
									
									  <div class="form-group  " >
										<label for="Dependant Name" class=" control-label col-md-4 text-left"> Dependant Name </label>
										<div class="col-md-7">
										  <input  type='text' name='dependantName' id='dependantName' value='{{ $row['dependantName'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Dependant Email" class=" control-label col-md-4 text-left"> Dependant Email </label>
										<div class="col-md-7">
										  <input  type='text' name='dependantEmail' id='dependantEmail' value='{{ $row['dependantEmail'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Dependant Contact No" class=" control-label col-md-4 text-left"> Dependant Contact No </label>
										<div class="col-md-7">
										  <input  type='text' name='dependantContactNo' id='dependantContactNo' value='{{ $row['dependantContactNo'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Dependant Qualification Level" class=" control-label col-md-4 text-left"> Dependant Qualification Level </label>
										<div class="col-md-7">
										  <select name='dependantQualificationLevel' rows='5' id='dependantQualificationLevel' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Dependant Interested Country" class=" control-label col-md-4 text-left"> Dependant Interested Country </label>
										<div class="col-md-7">
										  <select name='dependantInterestedCountry' rows='5' id='dependantInterestedCountry' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Dependant Faculty" class=" control-label col-md-4 text-left"> Dependant Faculty </label>
										<div class="col-md-7">
										  <input  type='text' name='dependantFaculty' id='dependantFaculty' value='{{ $row['dependantFaculty'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Dependant Occupation" class=" control-label col-md-4 text-left"> Dependant Occupation </label>
										<div class="col-md-7">
										  <input  type='text' name='dependantOccupation' id='dependantOccupation' value='{{ $row['dependantOccupation'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Dependant Father Name" class=" control-label col-md-4 text-left"> Dependant Father Name </label>
										<div class="col-md-7">
										  <input  type='text' name='dependantFatherName' id='dependantFatherName' value='{{ $row['dependantFatherName'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Dependant Mother Name" class=" control-label col-md-4 text-left"> Dependant Mother Name </label>
										<div class="col-md-7">
										  <input  type='text' name='dependantMotherName' id='dependantMotherName' value='{{ $row['dependantMotherName'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 
			</div>
			
			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('onlineapplicationsnewtwo?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#applicantQualificationLevel").jCombo("{!! url('onlineapplicationsnewtwo/comboselect?filter=ktmimmig_qualification_level:qualificationLevelName:qualificationLevelName') !!}",
		{  selected_value : '{{ $row["applicantQualificationLevel"] }}' });
		
		$("#applicantInterestedCountry").jCombo("{!! url('onlineapplicationsnewtwo/comboselect?filter=ktmimmig_countries:countryName:countryName') !!}",
		{  selected_value : '{{ $row["applicantInterestedCountry"] }}' });
		
		$("#dependantQualificationLevel").jCombo("{!! url('onlineapplicationsnewtwo/comboselect?filter=ktmimmig_qualification_level:qualificationLevelName:qualificationLevelName') !!}",
		{  selected_value : '{{ $row["dependantQualificationLevel"] }}' });
		
		$("#dependantInterestedCountry").jCombo("{!! url('onlineapplicationsnewtwo/comboselect?filter=ktmimmig_countries:countryName:countryName') !!}",
		{  selected_value : '{{ $row["dependantInterestedCountry"] }}' });
		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("onlineapplicationsnewtwo/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop