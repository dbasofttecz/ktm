@extends('layouts.frontendMaster')
@section('contents')
@if(Auth::check())
<!--asdhf-->
{!! Form::open(array('url'=>'onlineapplicationsnewtwo/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
@if(Session::has('messagetext'))
{!! Session::get('messagetext') !!}
@endif
<ul class="parsley-error-list">
   @foreach($errors->all() as $error)
   <li>{{ $error }}</li>
   @endforeach
</ul> 
<ul class="nav nav-tabs">
   <li class="active"><a href="#OnlineApplicationsStudent" data-toggle="tab">Online Applications Student</a></li>
   <li class=""><a href="#OnlineApplicationsDependant" data-toggle="tab">Online Applications Dependant</a></li>
</ul>
<div class="tab-content">
<div class="tab-pane m-t active" id="OnlineApplicationsStudent">
   <input type="hidden" name="id"   />               
   <div class="form-group  " >
      <label for="Applicant Name" class=" control-label col-md-4 text-left"> Applicant Name <span class="asterix"> * </span></label>
      <div class="col-md-7">
         <input  type='text' name='applicantName' id='applicantName' 
            required     class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Applicant Gender" class=" control-label col-md-4 text-left"> Applicant Gender <span class="asterix"> * </span></label>
      <div class="col-md-7">
         <label class='radio radio-inline'>
         <input type='radio' name='applicantGender' value ='M' required> Male </label>
         <label class='radio radio-inline'>
         <input type='radio' name='applicantGender' value ='F' required> Female </label> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Applicant Email" class=" control-label col-md-4 text-left"> Applicant Email <span class="asterix"> * </span></label>
      <div class="col-md-7">
         <input  type='text' name='applicantEmail' id='applicantEmail'
            required     class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Applicant Contact No" class=" control-label col-md-4 text-left"> Applicant Contact No <span class="asterix"> * </span></label>
      <div class="col-md-7">
         <input  type='text' name='applicantContactNo' id='applicantContactNo'  
            required     class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Applicant Qualification Level" class=" control-label col-md-4 text-left"> Applicant Qualification Level <span class="asterix"> * </span></label>
      <div class="col-md-7">
         <select name='applicantQualificationLevel' rows='5' id='applicantQualificationLevel' class='select2 ' required  >
            @foreach($qualificationLevel as $row)
            <option value="{{$row->qualificationLevelName}}">{{$row->qualificationLevelName}}</option>
            @endforeach
         </select> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Applicant Interested Country" class=" control-label col-md-4 text-left"> Applicant Interested Country <span class="asterix"> * </span></label>
      <div class="col-md-7">
         <select name='applicantInterestedCountry' rows='5' id='applicantInterestedCountry' class='select2 ' required  >
             @foreach($countriesList as $row)
            <option value="{{$row->countryName}}">{{$row->countryName}}</option>
            @endforeach
         </select> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Applicant Ielts Marks" class=" control-label col-md-4 text-left"> Applicant Ielts Marks </label>
      <div class="col-md-7">
         <input  type='text' name='applicantIeltsMarks' id='applicantIeltsMarks' 
            class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Applicant Nat Marks" class=" control-label col-md-4 text-left"> Applicant Nat Marks </label>
      <div class="col-md-7">
         <input  type='text' name='applicantNatMarks' id='applicantNatMarks' 
            class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Applicant Sat Marks" class=" control-label col-md-4 text-left"> Applicant Sat Marks </label>
      <div class="col-md-7">
         <input  type='text' name='applicantSatMarks' id='applicantSatMarks' 
            class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Applicant Gre Marks" class=" control-label col-md-4 text-left"> Applicant Gre Marks </label>
      <div class="col-md-7">
         <input  type='text' name='applicantGreMarks' id='applicantGreMarks' 
            class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Applicant Gmat Marks" class=" control-label col-md-4 text-left"> Applicant Gmat Marks </label>
      <div class="col-md-7">
         <input  type='text' name='applicantGmatMarks' id='applicantGmatMarks'
            class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Applicant Faculty" class=" control-label col-md-4 text-left"> Applicant Faculty <span class="asterix"> * </span></label>
      <div class="col-md-7">
         <input  type='text' name='applicantFaculty' id='applicantFaculty' 
            required     class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Applicant Occupation" class=" control-label col-md-4 text-left"> Applicant Occupation <span class="asterix"> * </span></label>
      <div class="col-md-7">
         <input  type='text' name='applicantOccupation' id='applicantOccupation' 
            required     class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Applicant Father Name" class=" control-label col-md-4 text-left"> Applicant Father Name <span class="asterix"> * </span></label>
      <div class="col-md-7">
         <input  type='text' name='applicantFatherName' id='applicantFatherName'
            required     class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Applicant Mother Name" class=" control-label col-md-4 text-left"> Applicant Mother Name <span class="asterix"> * </span></label>
      <div class="col-md-7">
         <input  type='text' name='applicantMotherName' id='applicantMotherName'
            required     class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="MartialStatus" class=" control-label col-md-4 text-left"> MartialStatus <span class="asterix"> * </span></label>
      <div class="col-md-7">
         <label class='radio radio-inline'>
         <input type='radio' name='martialStatus' value ='married' required> Married </label>
         <label class='radio radio-inline'>
         <input type='radio' name='martialStatus' value ='single' required> Single </label> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Language Certified Documents" class=" control-label col-md-4 text-left"> Language Certified Documents <span class="asterix"> * </span></label>
      <div class="col-md-7">
         <div class="language_certified_documentsUpl">   
            <input  type='file' name='language_certified_documents[]'  />        
         </div>
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Academic Documents" class=" control-label col-md-4 text-left"> Academic Documents <span class="asterix"> * </span></label>
      <div class="col-md-7">
         <div class="academic_documentsUpl"> 
            <input  type='file' name='academic_documents[]'  />         
         </div>
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Financial Documents" class=" control-label col-md-4 text-left"> Financial Documents <span class="asterix"> * </span></label>
      <div class="col-md-7">
         <div class="financial_documentsUpl">   
            <input  type='file' name='financial_documents[]'  />        
         </div>
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Work Experience Documents" class=" control-label col-md-4 text-left"> Work Experience Documents </label>
      <div class="col-md-7">
         <div class="work_experience_documentsUpl">   
            <input  type='file' name='work_experience_documents[]'  />        
         </div>
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Additional Documents" class=" control-label col-md-4 text-left"> Additional Documents </label>
      <div class="col-md-7">
         <div class="additional_documentsUpl">  
            <input  type='file' name='additional_documents[]'  />       
         </div>
      </div>
      <div class="col-md-1">
      </div>
   </div>
</div>
<div class="tab-pane m-t " id="OnlineApplicationsDependant">
   <div class="form-group  " >
      <label for="Dependant Name" class=" control-label col-md-4 text-left"> Dependant Name </label>
      <div class="col-md-7">
         <input  type='text' name='dependantName' id='dependantName' 
         class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Dependant Email" class=" control-label col-md-4 text-left"> Dependant Email </label>
      <div class="col-md-7">
         <input  type='text' name='dependantEmail' id='dependantEmail'
         class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Dependant Contact No" class=" control-label col-md-4 text-left"> Dependant Contact No </label>
      <div class="col-md-7">
         <input  type='text' name='dependantContactNo' id='dependantContactNo'
         class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Dependant Qualification Level" class=" control-label col-md-4 text-left"> Dependant Qualification Level </label>
      <div class="col-md-7">
         <select name='dependantQualificationLevel' rows='5' id='dependantQualificationLevel' class='select2 '   >
            @foreach($qualificationLevel as $row)
            <option value="{{$row->qualificationLevelName}}">{{$row->qualificationLevelName}}</option>
            @endforeach
         </select> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Dependant Interested Country" class=" control-label col-md-4 text-left"> Dependant Interested Country </label>
      <div class="col-md-7">
         <select name='dependantInterestedCountry' rows='5' id='dependantInterestedCountry' class='select2 '   >
            @foreach($countriesList as $row)
            <option value="{{$row->countryName}}">{{$row->countryName}}</option>
            @endforeach
         </select> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Dependant Faculty" class=" control-label col-md-4 text-left"> Dependant Faculty </label>
      <div class="col-md-7">
         <input  type='text' name='dependantFaculty' id='dependantFaculty' 
         class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Dependant Occupation" class=" control-label col-md-4 text-left"> Dependant Occupation </label>
      <div class="col-md-7">
         <input  type='text' name='dependantOccupation' id='dependantOccupation' 
         class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Dependant Father Name" class=" control-label col-md-4 text-left"> Dependant Father Name </label>
      <div class="col-md-7">
         <input  type='text' name='dependantFatherName' id='dependantFatherName' 
         class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Dependant Mother Name" class=" control-label col-md-4 text-left"> Dependant Mother Name </label>
      <div class="col-md-7">
         <input  type='text' name='dependantMotherName' id='dependantMotherName' 
         class='form-control ' /> 
      </div>
      <div class="col-md-1">
      </div>
   </div>
      <div class="form-group  " >
      <label for="Language Certified Documents" class=" control-label col-md-4 text-left"> Language Certified Documents <span class="asterix"> * </span></label>
      <div class="col-md-7">
         <div class="language_certified_documentsUpl">   
            <input  type='file' name='language_certified_documents[]'  />        
         </div>
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Academic Documents" class=" control-label col-md-4 text-left"> Academic Documents <span class="asterix"> * </span></label>
      <div class="col-md-7">
         <div class="academic_documentsUpl"> 
            <input  type='file' name='academic_documents[]'  />         
         </div>
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Financial Documents" class=" control-label col-md-4 text-left"> Financial Documents <span class="asterix"> * </span></label>
      <div class="col-md-7">
         <div class="financial_documentsUpl">   
            <input  type='file' name='financial_documents[]'  />        
         </div>
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Work Experience Documents" class=" control-label col-md-4 text-left"> Work Experience Documents </label>
      <div class="col-md-7">
         <div class="work_experience_documentsUpl">   
            <input  type='file' name='work_experience_documents[]'  />        
         </div>
      </div>
      <div class="col-md-1">
      </div>
   </div>
   <div class="form-group  " >
      <label for="Additional Documents" class=" control-label col-md-4 text-left"> Additional Documents </label>
      <div class="col-md-7">
         <div class="additional_documentsUpl">  
            <input  type='file' name='additional_documents[]'  />       
         </div>
      </div>
      <div class="col-md-1">
      </div>
   </div>
</div>
<div style="clear:both"></div>
<div class="form-group">
   <label class="col-sm-4 text-right">&nbsp;</label>
   <div class="col-sm-8">  
      <button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> Apply Online</button>
      
   </div>
</div>
{!! Form::close() !!}
<!--//asdfas-->
@else
   <div class="container">
      <p style="margin-top:10px" class="alert alert-success">Please <a href="{{url('/studentLogin')}}">login</a> to apply. If you don't have account with us, you can <a href="{{url('/signUp')}}">register</a> an account from the menu above.</p>
   </div>
@endif
@endsection