@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left" >
	   		<a href="{{ url('onlineapplicationsnewtwo?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('onlineapplicationsnewtwo/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
			@endif 
					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('onlineapplicationsnewtwo/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('onlineapplicationsnewtwo/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Name</td>
						<td>{{ $row->applicantName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Gender</td>
						<td>{{ $row->applicantGender}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Email</td>
						<td>{{ $row->applicantEmail}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant ContactNo</td>
						<td>{{ $row->applicantContactNo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Qualification Level</td>
						<td>{{ $row->applicantQualificationLevel}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Interested Country</td>
						<td>{{ $row->applicantInterestedCountry}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Ielts Marks</td>
						<td>{{ $row->applicantIeltsMarks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Nat Marks</td>
						<td>{{ $row->applicantNatMarks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Sat Marks</td>
						<td>{{ $row->applicantSatMarks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Gre Marks</td>
						<td>{{ $row->applicantGreMarks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Gmat Marks</td>
						<td>{{ $row->applicantGmatMarks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Faculty</td>
						<td>{{ $row->applicantFaculty}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Occupation</td>
						<td>{{ $row->applicantOccupation}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Father Name</td>
						<td>{{ $row->applicantFatherName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Applicant Mother Name</td>
						<td>{{ $row->applicantMotherName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Dependant Name</td>
						<td>{{ $row->dependantName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Dependant Email</td>
						<td>{{ $row->dependantEmail}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Dependant Contact No</td>
						<td>{{ $row->dependantContactNo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Dependant Qualification Level</td>
						<td>{{ $row->dependantQualificationLevel}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Dependant Interested Country</td>
						<td>{{ $row->dependantInterestedCountry}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Dependant Faculty</td>
						<td>{{ $row->dependantFaculty}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Dependant Occupation</td>
						<td>{{ $row->dependantOccupation}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Dependant Father Name</td>
						<td>{{ $row->dependantFatherName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Dependant Mother Name</td>
						<td>{{ $row->dependantMotherName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Martial Status</td>
						<td>{{ $row->martialStatus}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Entry By</td>
						<td>{{ $row->entry_by}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Language Certified Documents</td>
						<td>{!! SiteHelpers::formatRows($row->language_certified_documents,$fields['language_certified_documents'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Academic Documents</td>
						<td>{!! SiteHelpers::formatRows($row->academic_documents,$fields['academic_documents'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Financial Documents</td>
						<td>{!! SiteHelpers::formatRows($row->financial_documents,$fields['financial_documents'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Work Experience Documents</td>
						<td>{!! SiteHelpers::formatRows($row->work_experience_documents,$fields['work_experience_documents'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Additional Documents</td>
						<td>{!! SiteHelpers::formatRows($row->additional_documents,$fields['additional_documents'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Process Status</td>
						<td>{{ $row->process_status}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Process Remarks</td>
						<td>{{ $row->process_remarks}} </td>
						
					</tr>
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop