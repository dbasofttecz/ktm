

		 {!! Form::open(array('url'=>'filedownloads/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> fileDownloads</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group  " >
										<label for="FileName" class=" control-label col-md-4 text-left"> FileName <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='fileName' id='fileName' value='{{ $row['fileName'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="FileDescription" class=" control-label col-md-4 text-left"> FileDescription <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <textarea name='fileDescription' rows='5' id='editor' class='form-control editor '  
						required >{{ $row['fileDescription'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="FileUrl" class=" control-label col-md-4 text-left"> FileUrl </label>
										<div class="col-md-7">
										  <input  type='file' name='fileUrl' id='fileUrl' @if($row['fileUrl'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['fileUrl'],'/uploads/filefordownloads/') !!}
						
						</div>					
					 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="FileType" class=" control-label col-md-4 text-left"> FileType <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
					<?php $fileType = explode(',',$row['fileType']);
					$fileType_opt = array( 'Learning Center' => 'Learning Center' ,  'Company Information' => 'Company Information' , ); ?>
					<select name='fileType' rows='5' required  class='select2 ' id='fileType'  > 
						<?php 
						foreach($fileType_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['fileType'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
