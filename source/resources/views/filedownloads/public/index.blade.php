@extends('layouts.frontendMaster')
@section('contents')
<div class="container" style="padding-top:25px;">
   <ul class="nav nav-tabs">
      <li class="active"><a href="#2a" data-toggle="tab">Learning Center</a>
      </li>
      <li>
         <a  href="#1a" data-toggle="tab">Company Information</a>
      </li>
      
   </ul>
   <div class="tab-content clearfix">
      <div class="tab-pane active" id="1a">
         <ol type="1">
         	@foreach($downloadsList as $row)
	         	@if($row->fileType == "Company Information")
	         		<li><a href="{{url('uploads/filefordownloads/'.$row->fileUrl)}}">{{$row->fileName}}</a></li>
	         	@endif	
         	@endforeach
         </ol>
      </div>
      <div class="tab-pane" id="2a">
         <ol type="1">
         	@foreach($downloadsList as $row)
	         	@if($row->fileType == "Learning Center")
	         		<li><a href="{{url('uploads/filefordownloads/'.$row->fileUrl)}}">{{$row->fileName}}</a></li>
	         	@endif	
         	@endforeach         
         </ol>
      </div>
   </div>
</div>
@endsection