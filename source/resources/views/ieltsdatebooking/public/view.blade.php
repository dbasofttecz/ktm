<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Test By</td>
						<td>{{ $row->testBy}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Preferred Ielts Date</td>
						<td>{{ $row->preferredIeltsDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Test Models</td>
						<td>{{ $row->testModels}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Test Center</td>
						<td>{{ $row->testCenter}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Email  Address</td>
						<td>{{ $row->emailAddress}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Given Name</td>
						<td>{{ $row->givenNames}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Last Name</td>
						<td>{{ $row->lastName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>First Language</td>
						<td>{{ $row->firstLanguage}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Country of Nationality</td>
						<td>{{ $row->countryOfNationality}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Date of Birth</td>
						<td>{{ $row->dateOfBirth}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Passport Number</td>
						<td>{{ $row->passportNumber}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Passport Expiry Date</td>
						<td>{{ $row->passportExpiryDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Passport Issue Date</td>
						<td>{{ $row->passportIssueDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Address</td>
						<td>{{ $row->address}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>City</td>
						<td>{{ $row->city}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Zip Postal Code</td>
						<td>{{ $row->zip_postal_code}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	