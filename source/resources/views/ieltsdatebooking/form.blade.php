@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		</div>
		<div class="sbox-tools " >
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 			
		</div> 

	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'ieltsdatebooking/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset><legend> ieltsDateBooking</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group  " >
										<label for="Test By" class=" control-label col-md-4 text-left"> Test By <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
					<?php $testBy = explode(',',$row['testBy']);
					$testBy_opt = array( 'British council' => 'British council' ,  'IDP Australia' => 'IDP Australia' , ); ?>
					<select name='testBy' rows='5' required  class='select2 ' id='testBy'  > 
						<?php 
						foreach($testBy_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['testBy'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Preferred Ielts Date" class=" control-label col-md-4 text-left"> Preferred Ielts Date <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('preferredIeltsDate', $row['preferredIeltsDate'],array('class'=>'form-control date','id'=>'preferredIeltsDate')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Test Models" class=" control-label col-md-4 text-left"> Test Models <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
					<?php $testModels = explode(',',$row['testModels']);
					$testModels_opt = array( 'Academic Training' => 'Academic Training' ,  'General Training' => 'General Training' , ); ?>
					<select name='testModels' rows='5' required  class='select2 ' id='testModels'  > 
						<?php 
						foreach($testModels_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['testModels'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Test Center" class=" control-label col-md-4 text-left"> Test Center <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
					<?php $testCenter = explode(',',$row['testCenter']);
					$testCenter_opt = array( 'Kathmandu' => 'Kathmandu' ,  'Pokhara' => 'Pokhara' ,  'Biratnagar' => 'Biratnagar' ,  'Chitwan' => 'Chitwan' ,  'Bhairahawa' => 'Bhairahawa' , ); ?>
					<select name='testCenter' rows='5' required  class='select2 ' id='testCenter'  > 
						<?php 
						foreach($testCenter_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['testCenter'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Email Address" class=" control-label col-md-4 text-left"> Email Address <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='emailAddress' id='emailAddress' value='{{ $row['emailAddress'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Given Name" class=" control-label col-md-4 text-left"> Given Name <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='givenNames' id='givenNames' value='{{ $row['givenNames'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Last Name" class=" control-label col-md-4 text-left"> Last Name <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='lastName' id='lastName' value='{{ $row['lastName'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="First Language" class=" control-label col-md-4 text-left"> First Language <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='firstLanguage' id='firstLanguage' value='{{ $row['firstLanguage'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Country of Nationality" class=" control-label col-md-4 text-left"> Country of Nationality <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='countryOfNationality' id='countryOfNationality' value='{{ $row['countryOfNationality'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Date of Birth" class=" control-label col-md-4 text-left"> Date of Birth <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('dateOfBirth', $row['dateOfBirth'],array('class'=>'form-control date','id'=>'dateOfBirth')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Passport Number" class=" control-label col-md-4 text-left"> Passport Number <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='passportNumber' id='passportNumber' value='{{ $row['passportNumber'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Passport Expiry Date" class=" control-label col-md-4 text-left"> Passport Expiry Date <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('passportExpiryDate', $row['passportExpiryDate'],array('class'=>'form-control date','id'=>'passportExpiryDate')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Passport Issue Date" class=" control-label col-md-4 text-left"> Passport Issue Date <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('passportIssueDate', $row['passportIssueDate'],array('class'=>'form-control date','id'=>'passportIssueDate')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Address" class=" control-label col-md-4 text-left"> Address <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='address' id='address' value='{{ $row['address'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="City" class=" control-label col-md-4 text-left"> City <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='city' id='city' value='{{ $row['city'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Zip Postal Code" class=" control-label col-md-4 text-left"> Zip Postal Code </label>
										<div class="col-md-7">
										  <input  type='text' name='zip_postal_code' id='zip_postal_code' value='{{ $row['zip_postal_code'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('ieltsdatebooking?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("ieltsdatebooking/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop