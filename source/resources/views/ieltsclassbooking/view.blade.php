@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left" >
	   		<a href="{{ url('ieltsclassbooking?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('ieltsclassbooking/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
			@endif 
					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('ieltsclassbooking/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('ieltsclassbooking/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>BranchId</td>
						<td>{{ $row->branchId}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>IntakeId</td>
						<td>{{ $row->intakeId}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>StudentDesc</td>
						<td>{{ $row->studentDesc}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Full Name</td>
						<td>{{ $row->fullname}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Temporary Address</td>
						<td>{{ $row->temporaryAddress}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Permanent Address</td>
						<td>{{ $row->permanentAddress}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Email</td>
						<td>{{ $row->email}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Academic Qualification</td>
						<td>{{ $row->academicQualification}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Country Choice</td>
						<td>{{ $row->countryChoice}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Education Gap </td>
						<td>{{ $row->educationGap}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Work Experience</td>
						<td>{{ $row->workExperience}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Preferred Class Shift</td>
						<td>{{ $row->preferredClassShift}} </td>
						
					</tr>
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop