<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>BranchId</td>
						<td>{{ $row->branchId}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>IntakeId</td>
						<td>{{ $row->intakeId}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>StudentDesc</td>
						<td>{{ $row->studentDesc}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Full Name</td>
						<td>{{ $row->fullname}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Temporary Address</td>
						<td>{{ $row->temporaryAddress}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Permanent Address</td>
						<td>{{ $row->permanentAddress}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Email</td>
						<td>{{ $row->email}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Academic Qualification</td>
						<td>{{ $row->academicQualification}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Country Choice</td>
						<td>{{ $row->countryChoice}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Education Gap </td>
						<td>{{ $row->educationGap}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Work Experience</td>
						<td>{{ $row->workExperience}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Preferred Class Shift</td>
						<td>{{ $row->preferredClassShift}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	