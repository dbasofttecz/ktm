@extends('layouts.frontendMaster')
@section('contents')
{!! Form::open(array('url'=>'ieltsclassbooking/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
@if(Session::has('messagetext'))
{!! Session::get('messagetext') !!}
@endif
<ul class="parsley-error-list">
   @foreach($errors->all() as $error)
   <li>{{ $error }}</li>
   @endforeach
</ul>
<div class="col-md-12">
   <fieldset>
      <legend> Ielts Class Booking</legend>
      <input type="number" style="display:none" name="id" value="" />					
      <div class="form-group  " >
         <label for="Full Name" class=" control-label col-md-4 text-left"> Full Name <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='text' name='fullname' id='fullname' 
            required     class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Temporary Address" class=" control-label col-md-4 text-left"> Temporary Address <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='text' name='temporaryAddress' id='temporaryAddress'
            required     class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Permanent Address" class=" control-label col-md-4 text-left"> Permanent Address <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='text' name='permanentAddress' id='permanentAddress'
            required     class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Email" class=" control-label col-md-4 text-left"> Email <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='text' name='email' id='email' 
            required     class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Academic  Qualification" class=" control-label col-md-4 text-left"> Academic  Qualification <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='file' name='academicQualification' id='academicQualification' required style='width:150px !important;'  />
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Country Choice" class=" control-label col-md-4 text-left"> Country Choice <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='text' name='countryChoice' id='countryChoice' 
            required     class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Education Gap (If Any)" class=" control-label col-md-4 text-left"> Education Gap (If Any) </label>
         <div class="col-md-7">
            <input  type='text' name='educationGap' id='educationGap'
            class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Work Experience" class=" control-label col-md-4 text-left"> Work Experience </label>
         <div class="col-md-7">
            <input  type='file' name='workExperience' id='workExperience' required style='width:150px !important;'  />
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Preferred Class Shift" class=" control-label col-md-4 text-left"> Preferred Class Shift <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <div class="input-group m-b" style="width:150px !important;">
               {!! Form::text('preferredClassShift', '',array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
               <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
         </div>
         <div class="col-md-1">
         </div>
      </div>
   </fieldset>
</div>
<div style="clear:both"></div>
<div class="form-group">
   <label class="col-sm-4 text-right">&nbsp;</label>
   <div class="col-sm-8">	
      <button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
      <button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
   </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
   $(document).ready(function() { 
   	
   	 
   
   	$('.removeCurrentFiles').on('click',function(){
   		var removeUrl = $(this).attr('href');
   		$.get(removeUrl,function(response){});
   		$(this).parent('div').empty();	
   		return false;
   	});		
   	
   });
</script>
@endsection