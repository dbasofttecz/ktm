@extends('layouts.frontendMaster')
@section('contents')
    <div class="container">
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3>@foreach($pageDetails as $row)
                {{$row->title}}
            @endforeach
        </h3>
    </div>

    @foreach($pageDetails as $row)
            <p>{!! $row->note !!}</p>
    @endforeach
    </div>
@endsection