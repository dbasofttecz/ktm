@extends('layouts.frontendMaster')
@section('contents')
	<div class="col-md-12">
		<h4 class="text-center">Recent Visa Success Detail</h4>
		@foreach($data_students_details as $row)
		<?php var_dump($row); ?>
			<h4>Student Name:</h4><br><p>{{$row->studentName}}</p><br>
			<h4>Student Photo:</h4><br>
			<p><img src="{{url('uploads/visaSuccess/'.$row->studentPhot)}}"/></p><br>
			<h4>Country:</h4><br>
			<p>{{$row->country}}</p><br>
			<h4>Test Name:</h4><br>
			<p>{{$row->test_name}}</p><br>
			<h4>Description</h4><br>
			<p><?php echo strip_tags($row->successStory); ?></p>
		@endforeach
	</div>
@endsection