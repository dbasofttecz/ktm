@extends('layouts.frontendMaster')
@section('contents')
<div class="viasuccess-list-item">
    <div class="container">
        <div class="row">
            @foreach($data_all as $list_of_all_detils)
            <div class="col-md-4">
                    <div class="image-student" style="background-image: url(uploads/visaSuccess/{{$list_of_all_detils->studentPhot}});  background-size: cover;
   width: 100%;
   height: 259px;
   background-position: 100% 39%;
   box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    ">
                    </div>
                <div class="list_item">
                    <h1>
                       Country: {{ $list_of_all_detils->studentName }}
                    </h1>
                    <span>
                       Test Name: {{ $list_of_all_detils->test_name }}
                    </span>
                    <span>
                        {{ $list_of_all_detils->country }}
                    </span>
                   <span>Description<p><?php echo strip_tags($list_of_all_detils->successStory); ?></p>
                </div>
            </div>
              
            @endforeach

        </div>
    </div>
</div>
@endsection
