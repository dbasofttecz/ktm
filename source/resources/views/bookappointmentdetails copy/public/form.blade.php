@extends('layouts.frontendMaster')
@section('contents')
 {!! Form::open(array('url'=>'bookappointmentdetails/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

@if(Session::has('messagetext'))

   {!! Session::get('messagetext') !!}

@endif
<ul class="parsley-error-list">
@foreach($errors->all() as $error)
	<li>{{ $error }}</li>
@endforeach
</ul>		


<div class="col-md-12">
						<fieldset><legend> bookAppointMentDetails</legend>			
									  <div class="form-group  " >
										<label for="Appointee Name" class=" control-label col-md-4 text-left"> Appointee Name <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='appointeeName' id='appointeeName'
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Appointee Email" class=" control-label col-md-4 text-left"> Appointee Email <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='appointeeEmail' id='appointeeEmail' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Appointee Contact No" class=" control-label col-md-4 text-left"> Appointee Contact No <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='appointeeContactNo' id='appointeeContactNo'
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Appointee Preference" class=" control-label col-md-4 text-left"> Appointee Preference <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <select name='appointeePreference' rows='5' id='appointeePreference' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Appointee Questions" class=" control-label col-md-4 text-left"> Appointee Questions </label>
										<div class="col-md-7">
										  <textarea name='appointeeQuestions' rows='5' id='editor' class='form-control editor '  
						 ></textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
@endsection	 
