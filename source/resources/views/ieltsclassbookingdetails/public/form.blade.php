@extends('layouts.frontendMaster')
@section('contents')

		 {!! Form::open(array('url'=>'ieltsclassbookingdetails/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		

<div class="container">
<div class="col-md-12">
						<fieldset><legend> ieltsClassBookingDetails</legend>
				<input type="hidden" name="id"/>				
									  <div class="form-group  " >
										<label for="Branch Name" class=" control-label col-md-4 text-left"> Branch Name <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <select name='branchId' rows='5' id='branchId' class='select2 ' required  >
										  	@foreach ($branchList as $row)
										  		<option value="{{$row->id}}">{{$row->branchName}}</option>
										  	@endforeach
										  </select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Intake Name" class=" control-label col-md-4 text-left"> Intake Name <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <select name='intakeId' rows='5' id='intakeId' class='select2 ' required  >
										  	@foreach ($intakeList as $row)
										  		<option value="{{$row->id}}">{{$row->intakeName}}</option>
										  	@endforeach
										  </select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Student Description" class=" control-label col-md-4 text-left"> Student Description <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <textarea name='studentDesc' rows='5' id='editor' class='form-control editor '  
						required ></textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i>Submit</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
</div>		 
@endsection			 
