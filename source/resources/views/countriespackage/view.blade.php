@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left" >
	   		<a href="{{ url('countriespackage?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('countriespackage/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
			@endif 
					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('countriespackage/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('countriespackage/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Package Name (In Eng)</td>
						<td>{{ $row->packageName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Package Description (In Eng)</td>
						<td>{{ $row->packageDescription}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Featured Image</td>
						<td>{!! SiteHelpers::formatRows($row->featuredImage,$fields['featuredImage'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Destination Country Category</td>
						<td>{{ $row->countryId}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Package Name (In Nep)</td>
						<td>{{ $row->nepPackageName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Package Description (In Nep)</td>
						<td>{{ $row->nepPackageDescription}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Enable Translate</td>
						<td>{{ $row->enableTranslate}} </td>
						
					</tr>
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop