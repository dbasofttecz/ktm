<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Package Name (In Eng)</td>
						<td>{{ $row->packageName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Package Description (In Eng)</td>
						<td>{{ $row->packageDescription}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Featured Image</td>
						<td>{!! SiteHelpers::formatRows($row->featuredImage,$fields['featuredImage'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Destination Country Category</td>
						<td>{{ $row->countryId}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Package Name (In Nep)</td>
						<td>{{ $row->nepPackageName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Package Description (In Nep)</td>
						<td>{{ $row->nepPackageDescription}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Enable Translate</td>
						<td>{{ $row->enableTranslate}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	