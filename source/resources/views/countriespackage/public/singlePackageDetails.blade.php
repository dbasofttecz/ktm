@extends('layouts.frontendMaster')
@section('contents')
<!-- <div class="bg-image page-title">
	<div class="container-fluid">
		<a href="#"><h1>Country Details</h1></a>
		<div class="pull-right">
			<a href="01_home.html"><i class="fa fa-home fa-lg"></i></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="05_services.html">Our services</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="08_services-details.html">Country Details</a>
		</div>
	</div>
</div> -->

<div class="container-fluid block-content">
	<div class="row">
		<div class="col-sm-9 main-content">
		@foreach($rowData as $row)
		@if($row->enableTranslate == "disabled")
			<h1 class="text-center italic wow zoomIn" data-wow-delay="0.3s">{{$row->packageName}}</h1>
		
			<div class="row">                    	
				<div class="col-xs-12 wow zoomIn" data-wow-delay="0.3s">
					{!! $row->packageDescription !!}
				</div>
			</div>
		@elseif($row->enableTranslate == "enabled")
			<ul class="nav nav-tabs">
			   <li class="active"><a href="#OnlineApplicationsStudent" data-toggle="tab">View in English</a></li>
			   <li class=""><a href="#OnlineApplicationsDependant" data-toggle="tab">View in Nepali</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane m-t active" id="OnlineApplicationsStudent">
				   <h1 style="padding-top:20px;" class="text-center italic wow zoomIn" data-wow-delay="0.3s">{{$row->packageName}}</h1>
		
					<div class="row">                    	
						<div class="col-xs-12 wow zoomIn" data-wow-delay="0.3s">
							{!! $row->packageDescription !!}
						</div>
					</div>
				</div>
				<div class="tab-pane m-t " id="OnlineApplicationsDependant">
				   <h1 style="padding-top:20px;" class="text-center italic wow zoomIn" data-wow-delay="0.3s">{{$row->nepPackageName}}</h1>
		
					<div class="row">                    	
						<div class="col-xs-12 wow zoomIn" data-wow-delay="0.3s">
							{!! $row->nepPackageDescription !!}
						</div>
					</div>
				</div>
				<div style="clear:both"></div>
			</div>

		@endif	
		@endforeach
		</div>
		<div class="col-sm-3">
			<div class="sidebar-container">
				<div class="wow slideInUp" data-wow-delay="0.3s">
					<ul class="styled">
					@foreach($rowData1 as $row)	
						<li <?php if($packageId == $row->id) { echo "class='active'"; }?>><a href="{{ url('/singlePackageDetails/'.base64_encode($row->id).'/'.base64_encode($countryId)) }}">{{$row->packageName}}</a></li>
					@endforeach	
					</ul>
				</div>
			</div>
		</div>
	</div>            
</div>
@endsection
