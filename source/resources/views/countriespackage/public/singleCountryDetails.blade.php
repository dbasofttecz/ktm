@extends('layouts.frontendMaster')
@section('contents')
    <style>
        .service-item h4 {
            height: 34px;
        }

        .service-item p {
            height: 84px;
        }
    </style>
    <div class="bg-image">
        <div class="container" style="padding-top:25px;">
			<?php if(! empty( $banner_image )){ ?>
            @foreach($banner_image as $item)
                <img src="<?php echo url( 'uploads/countries/banners/' . $item->bannerImage ); ?>"
                     class="img-responsive"/>
            @endforeach
			<?php } ?>
        </div>
    </div>

    <div class="container block-content">
        <div class="row main-grid">
            <div class="col-sm-9">
                <div class="row services">
                    @foreach ($rowData as $rowes)
                        <div class="service-item col-xs-6 col-sm-4 col-md-4 col-lg-4 wow zoomIn" data-wow-delay="0.3s">
	                        <?php  $image_original = url('/uploads/countryPackages/'.$rowes->featuredImage); ?>
                            @if ($image_original)
		                        <?php  $image_dummies_replace = $image_original;  ?>
                            @else
		                        <?php $image_dummies_replace = asset( 'uploads/images/no-image.png' ); var_dump("asdadsadsa"); ?>
                            @endif
                            <img class="full-width" src="{!! $image_dummies_replace !!}" alt="Img">
                            <h4>{{$rowes->packageName}}</h4>
                            <p>{!! str_limit(strip_tags($rowes->packageDescription),150) !!}</p>
                            <a class="btn btn-default btn-sm"
                               href="{{ url('/singlePackageDetails/'.base64_encode($rowes->id).'/'.base64_encode($countryId)) }}">READ
                                MORE</a>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-sm-3 wow slideInUp" data-wow-delay="0.3s">
                <div class="sidebar-container">
                    <div>
                        <ul class="styled">
                            @foreach ($rowData1 as $row)
                                <li <?php if ( $row->id == $countryId ) {
									echo "class='active'";
								}?> >
                                    <a href="{{ url('/singleCountryDetails/'.base64_encode($row->id)) }}">{{$row->countryName}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection