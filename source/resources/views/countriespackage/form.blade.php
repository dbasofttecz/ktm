@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		</div>
		<div class="sbox-tools " >
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 			
		</div> 

	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'countriespackage/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset><legend> countriesPackage</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group  " >
										<label for="Package Name (In Eng)" class=" control-label col-md-4 text-left"> Package Name (In Eng) <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='packageName' id='packageName' value='{{ $row['packageName'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Package Description (In Eng)" class=" control-label col-md-4 text-left"> Package Description (In Eng) <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <textarea name='packageDescription' rows='5' id='editor' class='form-control editor '  
						required >{{ $row['packageDescription'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Featured Image" class=" control-label col-md-4 text-left"> Featured Image </label>
										<div class="col-md-7">
										  <input  type='file' name='featuredImage' id='featuredImage' @if($row['featuredImage'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['featuredImage'],'/uploads/countryPackages/') !!}
						
						</div>					
					 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Destination Country Category" class=" control-label col-md-4 text-left"> Destination Country Category <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <select name='countryId' rows='5' id='countryId' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Package Name (In Nep)" class=" control-label col-md-4 text-left"> Package Name (In Nep) </label>
										<div class="col-md-7">
										  <input  type='text' name='nepPackageName' id='nepPackageName' value='{{ $row['nepPackageName'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Package Description (In Nep)" class=" control-label col-md-4 text-left"> Package Description (In Nep) </label>
										<div class="col-md-7">
										  <textarea name='nepPackageDescription' rows='5' id='editor' class='form-control editor '  
						 >{{ $row['nepPackageDescription'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Enable Translate" class=" control-label col-md-4 text-left"> Enable Translate </label>
										<div class="col-md-7">
										  
					<label class='radio radio-inline'>
					<input type='radio' name='enableTranslate' value ='enabled'  @if($row['enableTranslate'] == 'enabled') checked="checked" @endif > Enable Translate </label>
					<label class='radio radio-inline'>
					<input type='radio' name='enableTranslate' value ='disabled'  @if($row['enableTranslate'] == 'disabled') checked="checked" @endif > Disable Translate </label> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('countriespackage?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#countryId").jCombo("{!! url('countriespackage/comboselect?filter=ktmimmig_countries:id:countryName') !!}",
		{  selected_value : '{{ $row["countryId"] }}' });
		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("countriespackage/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop