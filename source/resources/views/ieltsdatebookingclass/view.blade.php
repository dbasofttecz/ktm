@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left" >
	   		<a href="{{ url('ieltsdatebookingclass?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('ieltsdatebookingclass/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
			@endif 
					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('ieltsdatebookingclass/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('ieltsdatebookingclass/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Test By</td>
						<td>{{ $row->testBy}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Preferred Ielts Date</td>
						<td>{{ $row->preferredIeltsDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Test Models</td>
						<td>{{ $row->testModels}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Test Center</td>
						<td>{{ $row->testCenter}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Email Address</td>
						<td>{{ $row->emailAddress}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Given Name</td>
						<td>{{ $row->givenNames}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Last Name</td>
						<td>{{ $row->lastName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>First Language</td>
						<td>{{ $row->firstLanguage}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Country of Nationality</td>
						<td>{{ $row->countryOfNationality}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Date of Birth</td>
						<td>{{ $row->dateOfBirth}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Passport Number</td>
						<td>{{ $row->passportNumber}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Passport Expiry Date</td>
						<td>{{ $row->passportExpiryDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Passport Issue Date</td>
						<td>{{ $row->passportIssueDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Address</td>
						<td>{{ $row->address}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>City</td>
						<td>{{ $row->city}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Zip Postal Code</td>
						<td>{{ $row->zip_postal_code}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Mobile Number</td>
						<td>{{ $row->mobileNumber}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Telephone</td>
						<td>{{ $row->telephone}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Passport</td>
						<td>{{ $row->passport}} </td>
						
					</tr>
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop