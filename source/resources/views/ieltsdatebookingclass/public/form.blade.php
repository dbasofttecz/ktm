@extends('layouts.frontendMaster')
@section('contents')
{!! Form::open(array('url'=>'ieltsdatebooking/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
@if(Session::has('messagetext'))
{!! Session::get('messagetext') !!}
@endif
<ul class="parsley-error-list">
   @foreach($errors->all() as $error)
   <li>{{ $error }}</li>
   @endforeach
</ul>
<div class="col-md-12">
   <fieldset>
      <legend> Ielts Date Booking</legend>
      <input type="number" style="display:none" name="id" value="" />				
      <div class="form-group  " >
         <label for="Test By" class=" control-label col-md-4 text-left"> Test By <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <?php 
               $testBy_opt = array( 'British council' => 'British council' ,  'IDP Australia' => 'IDP Australia' , ); ?>
            <select name='testBy' rows='5' required  class='select2 ' id='testBy'  > 
            <?php 
               foreach($testBy_opt as $key=>$val)
               {
               	echo "<option  value ='$key'>$val</option>"; 						
               }						
               ?></select> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Preferred Ielts Date" class=" control-label col-md-4 text-left"> Preferred Ielts Date <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <div class="input-group m-b" style="width:150px !important;">
               {!! Form::text('preferredIeltsDate', '',array('class'=>'form-control date','id'=>'preferredIeltsDate')) !!}
               <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Test Models" class=" control-label col-md-4 text-left"> Test Models <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <?php
               $testModels_opt = array( 'Academic Training' => 'Academic Training' ,  'General Training' => 'General Training' , ); ?>
            <select name='testModels' rows='5' required  class='select2 ' id='testModels'  > 
            <?php 
               foreach($testModels_opt as $key=>$val)
               {
               	echo "<option  value ='$key'>$val</option>"; 						
               }						
               ?></select> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Test Center" class=" control-label col-md-4 text-left"> Test Center <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <?php 
               $testCenter_opt = array( 'Kathmandu' => 'Kathmandu' ,  'Pokhara' => 'Pokhara' ,  'Biratnagar' => 'Biratnagar' ,  'Chitwan' => 'Chitwan' ,  'Bhairahawa' => 'Bhairahawa' , ); ?>
            <select name='testCenter' rows='5' required  class='select2 ' id='testCenter'  > 
            <?php 
               foreach($testCenter_opt as $key=>$val)
               {
               	echo "<option  value ='$key'>$val</option>"; 						
               }						
               ?></select> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Email Address" class=" control-label col-md-4 text-left"> Email Address <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='text' name='emailAddress' id='emailAddress' 
               required     class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Given Name" class=" control-label col-md-4 text-left"> Given Name <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='text' name='givenNames' id='givenNames' 
               required     class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Last Name" class=" control-label col-md-4 text-left"> Last Name <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='text' name='lastName' id='lastName'
               required     class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="First Language" class=" control-label col-md-4 text-left"> First Language <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='text' name='firstLanguage' id='firstLanguage'
               required     class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Country of Nationality" class=" control-label col-md-4 text-left"> Country of Nationality <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='text' name='countryOfNationality' id='countryOfNationality' 
               required     class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Date of Birth" class=" control-label col-md-4 text-left"> Date of Birth <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <div class="input-group m-b" style="width:150px !important;">
               {!! Form::text('dateOfBirth', '',array('class'=>'form-control date','id'=>'dateOfBirth')) !!}
               <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Passport Number" class=" control-label col-md-4 text-left"> Passport Number <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='text' name='passportNumber' id='passportNumber' 
               required     class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Passport Expiry Date" class=" control-label col-md-4 text-left"> Passport Expiry Date <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <div class="input-group m-b" style="width:150px !important;">
               {!! Form::text('passportExpiryDate', '',array('class'=>'form-control date','id'=>'passportExpiryDate')) !!}
               <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Passport Issue Date" class=" control-label col-md-4 text-left"> Passport Issue Date <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <div class="input-group m-b" style="width:150px !important;">
               {!! Form::text('passportIssueDate','',array('class'=>'form-control date','id'=>'passportIssueDate')) !!}
               <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Address" class=" control-label col-md-4 text-left"> Address <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='text' name='address' id='address'
               required     class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="City" class=" control-label col-md-4 text-left"> City <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='text' name='city' id='city'
               required     class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Zip Postal Code" class=" control-label col-md-4 text-left"> Zip Postal Code </label>
         <div class="col-md-7">
            <input  type='text' name='zip_postal_code' id='zip_postal_code'
               class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
         <div class="form-group  " >
         <label for="Mobile Number" class=" control-label col-md-4 text-left"> Mobile Number <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='text' name='mobileNumber' id='mobileNumber'
            required     class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Telephone" class=" control-label col-md-4 text-left"> Telephone </label>
         <div class="col-md-7">
            <input  type='text' name='telephone' id='telephone'
            class='form-control ' /> 
         </div>
         <div class="col-md-1">
         </div>
      </div>
      <div class="form-group  " >
         <label for="Passport Image" class=" control-label col-md-4 text-left"> Passport Image <span class="asterix"> * </span></label>
         <div class="col-md-7">
            <input  type='file' name='passport' id='passport' required style='width:150px !important;'  />
         </div>
         <div class="col-md-1">
         </div>
      </div>
      </div>
   </fieldset>
</div>
<div style="clear:both"></div>
<div class="form-group">
   <label class="col-sm-4 text-right">&nbsp;</label>
   <div class="col-sm-8">	
      <button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> Book Now</button>
   </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
   $(document).ready(function() { 
   	
   	 
   
   	$('.removeCurrentFiles').on('click',function(){
   		var removeUrl = $(this).attr('href');
   		$.get(removeUrl,function(response){});
   		$(this).parent('div').empty();	
   		return false;
   	});		
   	
   });
</script>
@endsection			 
