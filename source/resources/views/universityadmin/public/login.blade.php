@extends('layouts.universityMaster')
@section('contents')
<div class="container text-center" >
<div class="row">
<div class="col-md-12 ">
<div class="col-md-thirty">
<form action="{{ url('universityloginprocess') }}" method="post">
    {{ csrf_field() }}
        @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <input name="email" class="form-control" placeholder="email" type="text">
    <input name="password" class="form-control" placeholder="password" type="password">
	  <div class="form-group">
    <button type="submit" class="btn btn-primary pull-right" ><i class="fa fa-sign-in"></i> {{ Lang::get('core.signin') }}</button>
    </div>

</form>


</div>

</div>
</div>
</div>

@endsection 
{{-- endsection --}}

