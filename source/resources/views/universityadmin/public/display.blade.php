@extends('layouts.universityMaster')
@section('contents')

<div class="container text-center" >
<div class="row">
<div class="col-md-12 ">

<?php $user = Auth::user(); ?>
{{-- @if(Auth::check()) --}}
@if($user->group_id == 6)
<h2>Add your univerisity details</h2>
<div class="col-md-thirty">
<form action="{{ url('univerisityentryprocess') }}" method="post">
    {{ csrf_field() }}

    <input name="univerisity_contry" class="form-control" placeholder="University Country" type="text" value="Study in ">
    <input name="universityname" class="form-control" placeholder="University name" type="text">
    <input class="form-control" name="location_univerisity" placeholder="University Location" type="text">
    <input class="form-control" name="establishdate" id="datepicker" placeholder="Establish Date">
		<input class="form-control" type="text" name="branches_university" placeholder="University Branch">
		<input class="form-control" type="number" name="no_of_students" placeholder="No of students">
		<div class="input_fields_wrap">
	    <div><input type="text" class="form-control" name="mytext[]" placeholder="Courses"></div>
	      <button class="add_field_button ">Add More Courses</button>
	  </div>
	  <div class="form-group">
    <input class="btn btn-primary" type="submit" name="click_result" value="Submit Univeristy">
    </div>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

</form>
</div>
@else
<h2>You are not allowed here !!!</h2>
<a href="{{url('universitylogin')}}" class="btn btn-warning">Login</a>
<a href="{{url('/')}}" class="btn btn-warning">Go Back</a>
@endif

</div>
</div>
</div>



@endsection
{{-- endsection --}}
