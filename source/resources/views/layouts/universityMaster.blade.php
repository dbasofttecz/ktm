<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8">
<![endif]-->
<!--[if IE 9]>
<html class="ie ie9">
<![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <title>Univeristy</title>

    <meta property="og:image" content="{{url('frontend/default/img/logo.png')}}">
    <!-- mobile settings -->
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0"/>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
    <![endif]-->
    <link rel="shortcut icon" href="{{asset('frontend/default/images/logo.ico')}}" type="image/x-icon">
    <!-- CSS -->
    {{--    <link rel="stylesheet" href="{{ asset('frontend/default/css/theme.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('frontend/default/css/master.css') }}">



    {{--<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet">--}}
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        window.$zopim || (function (d, s) {
            var z = $zopim = function (c) {
                z._.push(c)
            }, $ = z.s =
                d.createElement(s), e = d.getElementsByTagName(s)[0];
            z.set = function (o) {
                z.set._.push(o)
            };
            z._ = [];
            z.set._ = [];
            $.async = !0;
            $.setAttribute("charset", "utf-8");
            $.src = "https://v2.zopim.com/?4awRyR0Egoq8kI84hJQvRqjpgmm1eK6N";
            z.t = +new Date;
            $.type = "text/javascript";
            e.parentNode.insertBefore($, e)
        })(document, "script");
        $zopim(function () {
            $zopim.livechat.button.show();
        });
    </script>

    <style type="text/css">
        .zopim {
            display: none;
        }

        #at-custom-sidebar {
            top: 35% !important;
        }

        .demo1 {
            height: 398px !important;
            /*overflow-y: scroll !important;*/
            overflow: hidden;
        }

        .demo1 td {
            border-bottom: 1px solid #ccc;
        }
    </style>
</head>
<body>
<div class="sp-body">
    <!--bookanappointment-->
    <div id="bookanappointment">
        <a href="{{url('bookAppointment')}}"><img src="{{ asset('frontend') }}/default/img/book_appointment.png"/></a>
    </div>
    <!--//bookanappointment-->
    <!--livechat-->
    <div id="livechat">
        <a href="#" id="techSupport"><img src="{{ asset('frontend') }}/default/img/ask-the-expert-icon.png"/></a>
        <a href="#" id="askTheExpertClose">X</a>
    </div>
    <!--//livechat-->
    <!-- Loader Landing Page -->
{{--<div id="ip-container" class="ip-container">--}}
{{--<div class="ip-header">--}}
{{--<div class="ip-loader">--}}
{{--<svg class="ip-inner" width="60px" height="60px" viewBox="0 0 80 80">--}}
{{--<path class="ip-loader-circlebg"--}}
{{--d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,39.3,10z"/>--}}
{{--<path id="ip-loader-circle" class="ip-loader-circle"--}}
{{--d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>--}}
{{--</svg>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
<!-- Loader end -->
    <!-- navigation -->
  
    <!-- //navigation -->
    <!--contents-->
    <div class="container-fluid" style="background:#fff; box-shadow: 0px 3px 12px #888888;">
        <div class="row">
            @yield('contents')
        </div>
    </div>
    <!--//contents-->
    
</div>
<!-- /sp-body -->
<!-- Javascript -->
<!--Main-->
<script src="{{ asset('frontend/default/js/jquery-1.11.3.min.js') }}"></script>
<script src="{{ asset('frontend/default/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('frontend/default/js/modernizr.custom.js') }}"></script>
<!--scroller -->
{{--<script type="text/javascript" src="{{asset('/frontend/default/js/scroller.js')}}"></script>--}}
<!-- Loader -->
<script type="text/javascript" src="{{ asset('frontend/default/assets/isotope/jquery.isotope.min.js') }}"></script>
<!--Theme-->
<script src="{{ asset('frontend/default/js/jquery.smooth-scroll.js') }}"></script>
<script src="{{ asset('frontend/default/js/theme.js') }}"></script>


<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#techSupport').click(function (e) {
            e.preventDefault();
            $zopim.livechat.window.show();
        });

        $('#askTheExpertClose').click(function (e) {
            e.preventDefault();
            $('#livechat').hide();
            $zopim.livechat.window.hide();
        })
    });
</script>
<!--End of Zendesk Chat Script-->

<!-- Go to www.addthis.com/dashboard to customize your tools -->

</body>
</html>