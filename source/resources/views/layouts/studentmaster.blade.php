<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8">
<![endif]-->
<!--[if IE 9]>
<html class="ie ie9">
<![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <meta property="og:image" content="{{url('frontend/default/img/logo.png')}}">
    <!-- mobile settings -->
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0"/>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
    <![endif]-->
    <link rel="shortcut icon" href="{{asset('frontend/default/images/logo.ico')}}" type="image/x-icon">
    <!-- CSS -->
    {{--    <link rel="stylesheet" href="{{ asset('frontend/default/css/theme.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('frontend/default/css/master.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/default/css/unslider.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/default/css/unslider-dots.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/default/css/animate.css') }}">
    <link rel="stylesheet"
          href="{{ asset('sximo/js/plugins/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('sximo/js/plugins/datepicker/css/bootstrap-datetimepicker.min.css') }}">

    {{--<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet">--}}
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        window.$zopim || (function (d, s) {
            var z = $zopim = function (c) {
                z._.push(c)
            }, $ = z.s =
                d.createElement(s), e = d.getElementsByTagName(s)[0];
            z.set = function (o) {
                z.set._.push(o)
            };
            z._ = [];
            z.set._ = [];
            $.async = !0;
            $.setAttribute("charset", "utf-8");
            $.src = "https://v2.zopim.com/?4awRyR0Egoq8kI84hJQvRqjpgmm1eK6N";
            z.t = +new Date;
            $.type = "text/javascript";
            e.parentNode.insertBefore($, e)
        })(document, "script");
        $zopim(function () {
            $zopim.livechat.button.show();
        });
    </script>

    <style type="text/css">
        .zopim {
            display: none;
        }

        #at-custom-sidebar {
            top: 35% !important;
        }

        .demo1 {
            height: 398px !important;
            /*overflow-y: scroll !important;*/
            overflow: hidden;
        }

        .demo1 td {
            border-bottom: 1px solid #ccc;
        }
    </style>
</head>
<body>
<div class="sp-body">
    <!--bookanappointment-->
    <div id="bookanappointment">
        <a href="{{url('bookAppointment')}}"><img src="{{ asset('frontend') }}/default/img/book_appointment.png"/></a>
    </div>
    <!--//bookanappointment-->
    <!--livechat-->
    <div id="livechat">
        <a href="#" id="techSupport"><img src="{{ asset('frontend') }}/default/img/ask-the-expert-icon.png"/></a>
        <a href="#" id="askTheExpertClose">X</a>
    </div>
    <!--//livechat-->
    <!-- Loader Landing Page -->
{{--<div id="ip-container" class="ip-container">--}}
{{--<div class="ip-header">--}}
{{--<div class="ip-loader">--}}
{{--<svg class="ip-inner" width="60px" height="60px" viewBox="0 0 80 80">--}}
{{--<path class="ip-loader-circlebg"--}}
{{--d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,39.3,10z"/>--}}
{{--<path id="ip-loader-circle" class="ip-loader-circle"--}}
{{--d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>--}}
{{--</svg>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
<!-- Loader end -->
    <!-- navigation -->
    <header id="this-is-top">
        <div class="container">
            <div class="topmenu row">
                <nav id="topNav"
                     class="blockDisplay col-sm-offset-3 col-md-offset-4 col-lg-offset-4 col-sm-6 col-md-6 col-lg-6">
                    <a href="#testPreparation">Test Preparation</a>
                    <a href="{{url('/dispPages/our-branches')}}">Branch</a>
                    <a href="{{url('/dispPages/why-us')}}">Why Us ?</a>
                    <a href="{{url('downloads')}}">Downloads</a>
                    <!--singin-->
                     @if(Auth::check())
                        <a href="#"><i class="fa fa-user"></i>&nbsp;{{ Session::get('fid')}}</strong>
                        </a>
                        <a href="{{ url('user/logout') }}"><i class="glyphicon glyphicon-off"></i> Logout</a>

                    @else
                        <a href="{{url('studentLogin')}}">Sign In</a>
                        <a href="{{url('signUp')}}">Sign Up</a>
                @endif
                <!--//singin-->
                <!--  <a href="{{url('studentLogin')}}">Sign In</a> -->

                </nav>
                <nav class="text-right col-sm-3 col-md-2 col-lg-2">
                    <a class="social-icons" target="_blank" href="https://www.facebook.com/KTMIMMIGRATION/"><i
                                class="fa fa-facebook"></i></a>
                    <a class="social-icons" target="_blank" href="https://plus.google.com/u/1/108551602522247848911"><i
                                class="fa fa-google-plus"></i></a>
                    <a class="social-icons" target="_blank" href="https://twitter.com/KtmImmigration"><i
                                class="fa fa-twitter"></i></a>
                    <a class="social-icons" target="_blank"
                       href="https://www.youtube.com/channel/UCRZ-WtDYA9WcT26LzHM3-TQ"><i class="fa fa-youtube"></i></a>
                </nav>
            </div>
            <div class="row header">
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <a href="{{url('/')}}" id="logo"></a>
                </div>
                <div class="col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-sm-8 col-md-8 col-lg-8">
                    <div class="text-right header-padding" id="contactInfo">
                        <div class="h-block"><span>CALL US</span>+977-01-4426263</div>
                        <div class="h-block"><span>EMAIL US</span>immigrationktm@gmail.com</div>
                        <div class="h-block"><span>WORKING HOURS</span>Sun - Fri 10.00 - 5.00</div>
                        <a class="btn btn-success" href="{{url('applyOnlineNow')}}">Apply Online Now !</a>
                    </div>
                </div>
            </div>
            <div id="main-menu-bg"></div>
            <a id="menu-open" href="#"><i class="fa fa-bars"></i></a>
            <nav class="main-menu navbar-main-slide">
                <ul class="nav navbar-nav navbar-main">
                    <li>
                        <a href="{{url('/')}}">HOME</a>
                    </li>
                    <li>
                        <a href="{{url('/dispPages/about-us')}}">ABOUT US</a>
                    </li>
                    <li>
                        <a href="{{url('/allservices')}}">SERVICES</a>
                    </li>
                    {{--<li class="dropdown">--}}
                        {{--<a data-toggle="dropdown" class="dropdown-toggle border-hover-color1" href="#">OUR SERVICES <i--}}
                                    {{--class="fa fa-angle-down"></i></a>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--@foreach($our_services as $row)--}}
                                {{--<li>--}}
                                    {{--<a href="{{url('displayService/'.base64_encode($row->id))}}">{{$row->serviceName}}</a>--}}
                                {{--</li>--}}
                            {{--@endforeach--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    <li><a href="{{ url('/countriesList')}}">COUNTRY INFORMATION</a></li>
                    <li><a href="{{url('/universityList')}}">UNIVERSITY INFORMATION</a></li>
                    <li><a href="{{url('/blog')}}">Blog</a></li>
                    {{--<li class="displaynone"><a href="#testPreparation">Test Preparation</a></li>--}}
                    {{--<li class="displaynone"><a href="{{url('/dispPages/our-branches')}}">Branch</a></li>--}}
                    {{--<li class="displaynone"><a href="{{url('/dispPages/why-us')}}">Why Us ?</a></li>--}}
                    {{--<li class="displaynone"><a href="{{url('downloads')}}">Downloads</a></li>--}}
                    {{--<li class="displaynone"><a href="{{url('studentLogin')}}">Sign In</a></li>--}}
                    {{--<li class="displaynone"><a href="{{url('signUp')}}">Sign Up</a></li>--}}
                    {{--<li class="displaynone">--}}
                    {{--<li><a href="{{url('/dispPages/contact-us')}}">CONTACT US</a></li>--}}
                    {{--</li>--}}
                    <li><a class="btn_header_search" href="#"><i class="fa fa-search"></i></a></li>


                </ul>


                <div class="search-form-modal transition">
                    {!! Form::open(['method'=>'POST','url'=>'topSearchDetails','class'=>'navbar-form header_search_form','role'=>'search'])  !!}
                    <i class="fa fa-times search-form_close"></i>
                    <div class="form-group">
                        <input type="text" id="keywordsearch" name="keyword" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" id="click_top_search" class="btn btn_search customBgColor">Search</button>
                    {!! Form::close() !!}
                </div>
            </nav>
            <a id="menu-close" href="#"><i class="fa fa-times"></i></a>
        </div>
    </header>
    <!-- //navigation -->
    <!--contents-->
    <div class="container-fluid" style="background:#fff; box-shadow: 0px 3px 12px #888888;">
        <div class="row">
            @yield('contents')
        </div>
    </div>
    <!--//contents-->
    <footer>
        <div class="color-part2"></div>
        <div class="color-part"></div>
        <div class="container">
            <div class="row block-content">
                <div class="col-xs-8 col-sm-4 wow zoomIn" data-wow-delay="0.3s">
                    <a href="#" class="logo-footer" style="background:none;">KTM IMMIGRATION</a>
                    <p>We provide a wide range of services that guide students to effectively concentrate on their
                        dreams thereby us making the efforts to change those dreams into reality.</p>
                    <div class="footer-icons">
                        <a target="_blank" href="https://www.facebook.com/KTMIMMIGRATION/"><i
                                    class="fa fa-facebook-square fa-2x"></i></a>
                        <a target="_blank" href="https://plus.google.com/u/1/108551602522247848911"><i
                                    class="fa fa-google-plus-square fa-2x"></i></a>
                        <a target="_blank" href="https://twitter.com/KtmImmigration"><i
                                    class="fa fa-twitter-square fa-2x"></i></a>
                        <a target="_blank" href="https://www.youtube.com/channel/UCRZ-WtDYA9WcT26LzHM3-TQ"><i
                                    class="fa fa-youtube fa-2x"></i></a>
                    </div>
                    <a href="{{url('applyOnlineNow')}}" class="btn btn-lg btn-danger">APPLY ONLINE NOW</a>
                </div>
                <div class="col-xs-4 col-sm-2 wow zoomIn" data-wow-delay="0.3s">
                    <h4>OUR BRANCHES</h4>
                    
                </div>
                <div class="col-xs-6 col-sm-2 wow zoomIn" data-wow-delay="0.3s">
                    <h4>MAIN LINKS</h4>
                    <nav>
                        <a href="index.php">HOME</a>
                        <a href="#">ABOUT US</a>
                        <a href="#">OUR SERVICES</a>
                        <a href="#">COUNTRY INFORMATION</a>
                        <a href="#">UNIVERSITY INFORMATION</a>
                        <a href="#">CONTACT US</a>
                        <a href="#">BLOG</a>
                    </nav>
                </div>
                <div class="col-xs-6 col-sm-4 wow zoomIn" data-wow-delay="0.3s">
                    <h4>CONTACT INFO</h4>
                    <div class="contact-info">
                        <span><i class="fa fa-location-arrow"></i><strong>KTM IMMIGRATION PVT LTD.</strong><br>Putalisadak, Kathmandu, Nepal</span>
                        <span><i class="fa fa-phone"></i>+977-01-4426263</span>
                        <span><i class="fa fa-envelope"></i>immigrationktm@gmail.com </span>
                        <span><i class="fa fa-globe"></i>www.ktmimmigration.com.np</span>
                        <span><i class="fa fa-clock-o"></i>Sun - Fri  10.00 - 5.00</span>
                    </div>
                </div>
            </div>
            <div class="copy text-right"><a id="to-top" href="#this-is-top"><i class="fa fa-chevron-up"></i></a>Created
                by <a target="_blank" href="www.dbasofttech.com">DBASoftTech</a> &copy; 2017 KTM IMMIGRATION All rights
                reserved.
            </div>
        </div>
    </footer>
</div>
<!-- /sp-body -->
<!-- Javascript -->
<!--Main-->
<script src="{{ asset('frontend/default/js/jquery-1.11.3.min.js') }}"></script>
<script src="{{ asset('frontend/default/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('frontend/default/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('frontend/default/js/modernizr.custom.js') }}"></script>
<!--scroller -->
{{--<script type="text/javascript" src="{{asset('/frontend/default/js/scroller.js')}}"></script>--}}
<!-- Loader -->
<script src="{{ asset('frontend/default/assets/loader/js/classie.js') }}"></script>
<script src="{{ asset('frontend/default/assets/loader/js/pathLoader.js') }}"></script>
<script src="{{ asset('frontend/default/assets/loader/js/main.js') }}"></script>
<script src="{{ asset('frontend/default/js/classie.js') }}"></script>
<!--chosen-->
<script src="{{ asset('frontend/default/js/chosen.jquery.min.js') }}"></script>
<!--Switcher-->
<script src="{{ asset('frontend/default/assets/switcher/js/switcher.js') }}"></script>
<!--Owl Carousel-->
<script src="{{ asset('frontend/default/assets/owl-carousel/owl.carousel.min.js') }}"></script>
<!--Contact form-->
<!-- <script src="{{ asset('frontend') }}/default/assets/contact/jqBootstrapValidation.js"></script>
            <script src="{{ asset('frontend') }}/default/assets/contact/contact_me.js"></script>
      -->       <!-- SCRIPTS -->
<script type="text/javascript" src="{{ asset('frontend/default/assets/isotope/jquery.isotope.min.js') }}"></script>
<!--Theme-->
<script src="{{ asset('frontend/default/js/jquery.smooth-scroll.js') }}"></script>
<script src="{{ asset('frontend/default/js/wow.min.js') }}"></script>
<script src="{{ asset('frontend/default/js/jquery.placeholder.min.js') }}"></script>
{{--<script src="{{ asset('frontend/default/js/smoothscroll.min.js') }}"></script>--}}
<script src="{{ asset('frontend/default/js/theme.js') }}"></script>
<script src="{{ asset('sximo/js/plugins/datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('sximo/js/plugins/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('.selectpicker').chosen({width: "100%"});
        $('.date').datepicker({format: 'yyyy-mm-dd', autoClose: true})
        $('.time').datetimepicker({format: 'hh:ii:ss'});
    });
    var stickyNav = function () {
        var game = $(window).width();
        if (game <= 798) {
            $("#topNav").removeClass("blockDisplay");
            $("ul.navbar-main > li").removeClass("displaynone");
            $(window).scroll(function () {
                if ($(window).scrollTop() > 400) {
                    $('#menu-open').addClass('stuck');
                } else {
                    $('#menu-open').removeClass('stuck');
                }
            });
        }
    }
    stickyNav();
    $(window).resize(function () {
        stickyNav();
    });
</script>
<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#techSupport').click(function (e) {
            e.preventDefault();
            $zopim.livechat.window.show();
        });

        $('#askTheExpertClose').click(function (e) {
            e.preventDefault();
            $('#livechat').hide();
            $zopim.livechat.window.hide();
        })
    });
</script>
<!--End of Zendesk Chat Script-->

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".searchDestination #click_btn").click(function () {
            var select_field = jQuery(".searchDestination #validation").val();
            if (select_field === "") {
                jQuery(".searchDestination #validation").addClass("validation-color");
                jQuery(".error-msg").html("All three Field Required");
                return false;
            }

        });

        jQuery("#click_top_search").click(function () {
            var select_field_top = jQuery(".search-form-modal #keywordsearch").val();
            if (select_field_top === "") {
                alert("Search Input Field Required For Search");
                jQuery("#keywordsearch").addClass("validation-color");
                return false;
            }

        });
    });
</script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
{{--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58da31b365a6e288"></script>--}}
</body>
</html>