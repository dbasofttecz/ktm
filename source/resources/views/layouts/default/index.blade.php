@extends('layouts.frontendMaster')
@section('contents')
<!-- slider -->
<div class="owl-carousel enable-owl-carousel" data-auto-play="true" data-main-slider="true" data-pagination="false" data-single-item="true" data-stop-on-hover="true" id="owl-main-slider">
    @foreach($homePageSlideshow as $row)
    <div class="item">
        <img alt="Img" src="{{url('/uploads/slideshow/'.$row->slideImage)}}">
            <div class="container-fluid">
                <div class="slider-content col-md-6 col-lg-6">
                    <div style="display:table;">
                        <div style="display:table-cell; width:100px; vertical-align:top;">
                            <a class="prev">
                                <i class="fa fa-angle-left">
                                </i>
                            </a>
                            <a class="next">
                                <i class="fa fa-angle-right">
                                </i>
                            </a>
                        </div>
                        <div style="display:table-cell;">
                            <h1>
                                {{$row->slideName}}
                            </h1>
                        </div>
                    </div>
                    <p>
                        {!! strip_tags($row->slideDescription) !!}
                    </p>
                    <a class="btn btn-success" href="{{$row->slideLink}}">
                        LEARN MORE
                    </a>
                </div>
            </div>
        </img>
    </div>
    @endforeach
</div>
<!-- //slider -->
<div class="container block-content1 marg_25">
    <div class="row">
        <div class="col-sm-5 col-md-5 col-lg-5 wow fadeInLeft" data-wow-delay="0.3s">
            <!--destination_explore-->
            <div class="searchDestination">
                <h1 class="color-1 cust_marg">
                    Explore Your Study Options
                </h1>

                <form action="{{url('searchDetails')}}" method="post">
                    <select class="form-control" id="validation" name="searchDestination">
                        <option selected="" value="">
                            Country
                        </option>
                        @foreach($alllistrecord as $row)
                        <option style="text-transform: capitalize;" value="{{$row->univerisity_contry}}">
                            {{$row->univerisity_contry}}
                        </option>
                        @endforeach
                    </select>
                    <select class="form-control" id="validation" name="levelofcourse">
                        <option selected="" value="">
                            Level of Course you're looking for
                        </option>
                        @foreach($studyLevelList as $row)
                        <option style="text-transform: capitalize;" value="{{$row->studyLevelName}}">
                            {{$row->studyLevelName}}
                        </option>
                        @endforeach
                    </select>
                    <select class="form-control" id="validation" name="areaofinterest">
                        <option selected="" value="">
                            Your area of Interest
                        </option>
                        @foreach($alllistrecord as $row)
                        <option style="text-transform: capitalize;" value="{{$row->mytext}}">
                            {{$row->mytext}}
                        </option>
                        @endforeach
                    </select>
                    <button class="btn btn-danger" id="click_btn" type="submit">
                        SEARCH
                    </button>
                    <div class="error-msg">
                    </div>
                </form>
            </div>
            <!--//destination_explore-->
        </div>
        <div class="col-sm-7 col-md-7 col-lg-7 wow fadeInRight" data-wow-delay="0.3s">
            <div class="quote-form">
                <h1 class="color-1 cust_marg" style="margin-bottom:10px;">
                    welcome to ktmimmigration
                </h1>
                <p>
                    Preparing for IELTS exam ? Yet in dilemma whether you are qualified or not ? Test Yourself !
                </p>
                <a class="btn btn-success cust-btn" href="{{url('bookMockTest')}}" style="width:200px; margin-bottom:10px;">
                    Book Your Mock Test
                </a>
                <br>
                    <h1 class="color-1 cust_marg" style="margin-bottom:10px;">
                        BOOK YOUR IELTS DATE
                    </h1>
                    <p>
                        Do you want to book a date for IELTS exam? Send us all required documents and we will book IELTS
                        date on your behalf.
                    </p>
                    <a class="btn btn-success cust-btn" href="{{url('ieltsBookingForm')}}" style="width:150px;">
                        Book
                        Now
                    </a>
                </br>
            </div>
        </div>
    </div>
</div>
<div class="block-content inner-offset">
    <div class="info-texts wow fadeIn" data-wow-delay="0.3s">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-4 col-lg-4" style="height:120px;">
                    <a class="btn btn-success" href="{{url('bookIeltsClass')}}" style="z-index: 1000;
    background-color: #eb1a20;">
                        Book Ielts Class
                    </a>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-4" style="height:150px;">
                    <a class="btn btn-success" href="{{url('downloads')}}">
                        Visit Learning Centre
                    </a>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <p>
                        We provide Test Preparation services (IELTS, TOEFL, GRE, GMAT), Abroad Study Counselling and
                            Consultancy Services
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row column-info">
        @foreach($countriesList as $row)
        <div class="col-sm-4 col-md-4 col-lg-4 wow fadeInLeft marg_25" data-wow-delay="0.3s">
            <img alt="Img" src="{{url('/uploads/countries/'.$row->featuredImage)}}">
                <span>
                </span>
                <h3>
                    {{$row->countryName}}
                </h3>
                <p class="countrySummary">
                    {!! str_limit(strip_tags($row->countryDesc), 150) !!}
                </p>
                <a class="btn btn-default btn-sm" href="{{ url('/singleCountryDetails/'.base64_encode($row->id)) }}">
                    READ MORE
                </a>
            </img>
        </div>
        @endforeach
    </div>
</div>
<div class="big-hred">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="ribbon">
                    <strong class="ribbon-content">
                        <div class="text-center" style="margin-right:40px;">
                            <h2>
                                WE PROVIDE QUALITY COUNSELLING AND CONSULTANT SERVICES
                            </h2>
                            <p>
                                One of our highly trained counsellors will review your documents and will be in touch to
                                    discuss your study goals.
                            </p>
                        </div>
                    </strong>
                    <div class="row-content">
                        <a class="btn btn-danger btn-lg" href="{{url('applyOnlineNow')}}">
                            APPLY ONLINE NOW
                        </a>
                    </div>
                </h1>
            </div>
        </div>
    </div>
</div>
<div class="wrapper-testpreparation">
    <div class="container block-content">
        <div class="hgroup text-center wow zoomInUp" data-wow-delay="0.3s">
            <h1 id="testPreparation">
                TEST PREPARATION
            </h1>
            <h2>
                We provide preparation class services for various online tests as following.
            </h2>
        </div>



        <div class="row column-info" id="testPreparationContent">
          @foreach($display_illets as $list_known)
            <div class="col-sm-4 col-md-4 col-lg-4 wow zoomInLeft marg_25" data-wow-delay="0.3s">
                <img src="{{url('/uploads/testpreparation/'.$list_known->Image_test)}}" style="float: left; margin: 3px 9px;  max-width: 111px;"/>
                <p>
                    {{strip_tags($list_known->content_test)}}
                </p>
                <a class="btn btn-default btn-sm" href="  {{ url('/preparationdetails/'.base64_encode($list_known->id)) }}">
                    READ MORE
                </a>
            </div>
            @endforeach
        </div>
    </div>
</div>
<div class="container block-content">
    <div class="row">
        <div class="col-md-4 col-lg-4 wow fadeInLeft" data-wow-delay="0.3s">
            <div class="hgroup">
                <h1>
                    Student Testimonials
                </h1>
            </div>
            <div class="owl-carousel enable-owl-carousel" data-auto-play="true" data-navigation="true" data-pagination="false" data-single-item="true" id="testimonials">
                @foreach($testimonialList as $row)
                <div>
                    <div class="testimonial-content" style="padding:10px;">
                            <img class="img-responsive" height="65" src="{{url('/uploads/testimonialImg/'.$row->authorImage)}}" width="65">
                            </img>
                      
                       
                            <p>
                                {!! $row->testimonial !!}
                            </p>
                    </div>
                    <div class="text-right testimonial-author">
                        <h4>
                            {{$row->authorname}}
                        </h4>
                        <small>
                            {{$row->organization}}
                        </small>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-4 col-lg-4 wow zoomInUp" data-wow-delay="0.3s">
            <div class="hgroup">
                <h1 class="text-center">
                    Notice Board
                </h1>
            </div>
            <!--demo1-->
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            {{--
                            <div class="microsoft">
                                --}}
                                {{--
                                <div class="marquee">
                                    --}}
                                    <ul class="notice-board">
                                        <marquee direction="up" height="390">
                                            @foreach($noticeBoardList as $row)
                                            <li>
                                                <h1>
                                                    {{$row->noticeName}}
                                                </h1>
                                                <p>
                                                    {!! strip_tags($row->noticeDescription) !!}
                                                </p>
                                            </li>
                                            @endforeach
                                        </marquee>
                                    </ul>
                                    {{--
                                </div>
                                --}}
                                {{--
                            </div>
                            --}}
                        </div>
                    </div>
                </div>
            </div>
            <!--//demo1-->
        </div>
        <div class="col-md-4 col-lg-4 wow fadeInRight" data-wow-delay="0.3s">
            <div class="hgroup">
                <h1>
                    WHY CHOOSE US
                </h1>
            </div>
            <!--demo1-->
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="why-choose-us">
                                <marquee direction="up" height="390">
                                    @foreach($whyChooseUs as $row)
                                    <?php  $image_original = url( 'uploads/whyChooseUs/' . $row->
                                    featuredImage ); ?>
                                            @if ($image_original)
                                    <?php  $image_dummies_replace = url( 'uploads/whyChooseUs/' . $row->
                                    featuredImage );  ?>
                                            @else
                                    <?php $image_dummies_replace = asset( 'uploads/images/no-image.png' ); ?>
                                    @endif
                                    <li class="news-item">
                                        <img src="{{$image_dummies_replace}}">
                                            {{$row->whyUs}}
                                            <p>
                                                {!! strip_tags( $row->whyUsDescription ); !!}
                                            </p>
                                        </img>
                                    </li>
                                    @endforeach
                                </marquee>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--//demo1-->
        </div>
    </div>
</div>
<div class="container block-content">
    <div class="row">
        <div class="col-md-4 col-lg-4 wow fadeInLeft" data-wow-delay="0.3s">
            <div class="hgroup">
                <h1 class="text-center">
                    Latest News
                </h1>
            </div>
            <!--demo1-->
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul style="padding-left: 0px; list-style: none;">
                                <marquee direction="up" height="390">
                                    @foreach($latest_news as $row)
                                    <li class="news-item recent-news-lists">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td style="padding:10px" width="70%">
                                                    <a href="{{ url('/viewNews/'.base64_encode($row->id)) }}">
                                                        <h1>
                                                            {{$row->newsTitle}}
                                                        </h1>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                    @endforeach
                                </marquee>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--//demo1-->
        </div>
        <div class="col-md-4 col-lg-4 wow zoomInUp" data-wow-delay="0.3s">
            <div class="hgroup">
                <h1 class="text-center">
                    Recent Visa Success
                </h1>
            </div>
            <!--demo1-->
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="demo1" style="padding-left: 0px; list-style: none;">
                                <marquee direction="up" height="390">
                                    @foreach($ktimmig_visa_success as $row)
                                    <li class="news-item student-details">
                                        <table cellpadding="4" cellspacing="10" width="100%">
                                            <tr>
                                                <td width="20%">
                                                    <img src="{{url('uploads/visaSuccess/'.$row->studentPhot)}}"/>
                                                </td>
                                                <td class="studentDetails" style="padding:10px" width="80%">
                                                    <h1>
                                                        Student Name :
                                                        <span>
                                                            {{$row->studentName}}
                                                        </span>
                                                    </h1>
                                                    <p>
                                                        Country Visa : {{$row->country}}
                                                    </p>
                                                    {{--
                                                    <h5>
                                                        Country : {{$row->country}}
                                                    </h5>
                                                    --}}
                                                            {{--
                                                    <a href="{{url('recentVisaSuccessDetails/'.$row->id)}}">
                                                        Read More >>
                                                    </a>
                                                    --}}
                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                    @endforeach
                                </marquee>
                            </ul>
                                      <div class="text-center">
                                    <a class="btn btn-default btn-sm" href="{{url('visasuccesslist')}}">
                                        View
                                            all
                                    </a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--//demo1-->
        </div>
        <div class="col-md-4 col-lg-4 wow fadeInRight" data-wow-delay="0.3s">
            <div class="hgroup">
                <h1>
                    Recent Test Score
                </h1>
            </div>
            <!--demo1-->
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="recent-score">
                                <marquee direction="up" height="363">
                                    @foreach($ktmimmig_recent_test_score as $row)
                                    <li class="news-item visa-success">
                                        <img class="img-responsive" src="{{url('uploads/recentScore/'.$row->studentPhoto)}}">
                                            <div class="all-content">
                                                <span>
                                                    Student Name : {{$row->studentName}}
                                                </span>
                                                <br>
                                                    <span>
                                                        Test Name : {{$row->testName}}
                                                    </span>
                                                    <br>
                                                        <span>
                                                            Test Score : {{$row->testScore}}
                                                        </span>
                                                        <br>
                                                        </br>
                                                    </br>
                                                </br>
                                            </div>
                                        </img>
                                    </li>
                                    @endforeach
                                </marquee>
                                <div class="text-center">
                                    <a class="btn btn-default btn-sm" href="{{url('recentTestScoreDetails')}}">
                                        View
                                            all
                                    </a>
                                </div>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--//demo1-->
        </div>
    </div>
</div>
<div class="container-rows-fluid">
    <div class="container partners block-content">
        <div class="title-space wow fadeInLeft" data-wow-delay="0.3s">
            <div class="hgroup">
                <h1>
                    WE ARE MEMBER OF
                </h1>
            </div>
        </div>
        <div class="owl-carousel enable-owl-carousel" data-auto-play="true" data-min450="2" data-min600="2" data-min768="4" data-navigation="true" data-pagination="false" data-stop-on-hover="true" id="partners">
            @foreach($membership as $row)
            <div class="wow rotateIn" data-wow-delay="0.3s">
                <a href="{{$row->website}}">
                    <img alt="Img" src="{{url('/uploads/membershiplogo/'.$row->organizationLogo)}}"/>
                </a>
                <h6>
                    {{$row->organizationName}}
                </h6>
            </div>
            @endforeach
        </div>
    </div>
</div>
<div class="container partners block-content">
    <div class="title-space wow fadeInLeft" data-wow-delay="0.3s">
        <div class="hgroup">
            <h1>LIST OF UNIVERSITY</h1>
        </div>
    </div>
    <div class="owl-carousel enable-owl-carousel" data-auto-play="true" data-min450="2" data-min600="2" data-min768="4" data-navigation="true" data-pagination="false" data-stop-on-hover="true" id="partners">
        @foreach($universitylist as $row)
        <div class="wow rotateIn" data-wow-delay="0.3s">
            <a href="{{url('/singleUniDetails/'.base64_encode($row->id))}}">
                <img alt="Img" src="{{url('/uploads/unilogo/'.$row->universityLogo)}}"/>
            </a>
            <h6>
                {{$row->universityName}}
            </h6>
        </div>
        @endforeach
    </div>
</div>
@endsection
