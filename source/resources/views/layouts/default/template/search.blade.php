@extends('layouts.frontendMaster')
@section('contents')
    <div class="col-md-12 extraforsearch">
        <h4 class="text-center">Search Results</h4>
    </div>
    <div class="container">

        @foreach($searchListDetails as $row)
            <div class="col-md-4 wdith-img-responsive">
                <a href="{{url('/singleUniDetails/'.base64_encode($row->id))}}">
                    <div class="image-res"
                         style="background-image: url({{url('/uploads/unilogo/'.$row->universityLogo)}})"></div>
                    <h4>{{$row->universityName}}</h4>
                </a>
                <p>{{$row->universityCountry}}</p>
            </div>
        @endforeach
        @endsection
    </div>