@extends('layouts.frontendMaster')
@section('contents')
<div class="container">

		 {!! Form::open(array('url'=>'bookAppointmentPost', 'class'=>'form-horizontal frontendForm','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> bookAppointMentDetails</legend>
				{!! Form::hidden('id', '') !!}					
									  <div class="form-group  " >
										<label for="Appointee Name" class=" control-label col-md-4 text-left"> Appointee Name <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='appointeeName' id='appointeeName'
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Appointee Email" class=" control-label col-md-4 text-left"> Appointee Email <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' parsley-type="email" name='appointeeEmail' id='appointeeEmail' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Appointee Contact No" class=" control-label col-md-4 text-left"> Appointee Contact No <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='appointeeContactNo' id='appointeeContactNo' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Appointee Preference" class=" control-label col-md-4 text-left"> Appointee Preference <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <select name='appointeePreference' rows='5' id='appointeePreference' class='select2 '  >
										  	@foreach($countries as $row)
										  	<option value="{{$row->id}}">{{$row->countryName}}</option>
										  	@endforeach
										  </select> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Appointee Questions" class=" control-label col-md-4 text-left"> Appointee Questions </label>
										<div class="col-md-7">
										  <textarea name='appointeeQuestions' rows='5' id='editor' class='form-control editor '  
						 ></textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> Submit</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#appointeePreference").jCombo("{!! url('bookappointmentdetails/comboselect?filter=ktmimmig_countries:id:countryName') !!}",
		{  selected_value : '' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>
</div>	
@endsection		 
