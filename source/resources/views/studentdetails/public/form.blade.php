@extends('layouts.frontendMaster')
@section('contents')
    <div class="container">
        {!! Form::open(array('url'=>'studentSignupPost', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

        @if(Session::has('messagetext'))

            {!! Session::get('messagetext') !!}

        @endif
        <ul class="parsley-error-list">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>


        <div class="col-md-12">
            <fieldset>
                <legend> studentDetails</legend>

                <div class="form-group" style="display: none;">
                    <label for="Id" class=" control-label col-md-4 text-left"> Id </label>
                    <div class="col-md-7">
                        <input type='text' name='id' id='id' value="" class='form-control '/>
                    </div>
                    <div class="col-md-1">

                    </div>
                </div>
   {{--              <div class="form-group  ">
                    <label for="Passport No" class=" control-label col-md-4 text-left"> Passport No <span
                                class="asterix"> * </span></label>
                    <div class="col-md-7">
                        <input type='text' name='passport_no' id='passport_no'
                               required class='form-control '/>
                    </div>
                    <div class="col-md-1">

                    </div>
                </div> --}}
     {{--            <div class="form-group  ">
                    <label for="Username" class=" control-label col-md-4 text-left"> Username <span
                                class="asterix"> * </span></label>
                    <div class="col-md-7">
                        <input type='text' name='username' id='username'
                               required class='form-control '/>
                    </div>
                    <div class="col-md-1">

                    </div>
                </div> --}}
                <div class="form-group  ">
                    <label for="Password" class=" control-label col-md-4 text-left"> Password <span
                                class="asterix"> * </span></label>
                    <div class="col-md-7">
                        <input type='password' name='password' id='password' class='form-control '/>
                    </div>
                    <div class="col-md-1">

                    </div>
                </div>
                <div class="form-group  ">
                    <label for="Password" class=" control-label col-md-4 text-left"> Confirm Password <span
                                class="asterix"> * </span></label>
                    <div class="col-md-7">
                        <input type='password' name='password_confirmation' id='password' class='form-control '/>
                    </div>
                    <div class="col-md-1">

                    </div>
                </div>
                <div class="form-group  ">
                    <label for="Email" class=" control-label col-md-4 text-left"> Email <span class="asterix"> * </span></label>
                    <div class="col-md-7">
                        <input type='text' name='email' id='email'
                               required class='form-control '/>
                    </div>
                    <div class="col-md-1">

                    </div>
                </div>
                <div class="form-group  ">
                    <label for="First Name" class=" control-label col-md-4 text-left"> First Name <span class="asterix"> * </span></label>
                    <div class="col-md-7">
                        <input type='text' name='firstname' id='first_name'
                               required class='form-control '/>
                    </div>
                    <div class="col-md-1">

                    </div>
                </div>
                <div class="form-group  ">
                    <label for="Last Name" class=" control-label col-md-4 text-left"> Last Name <span
                                class="asterix"> * </span></label>
                    <div class="col-md-7">
                        <input type='text' name='lastname' id='last_name'
                               required class='form-control '/>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>

                <div class="form-group  ">
                    <label for="Permanent Address" class=" control-label col-md-4 text-left"> Address <span
                                class="asterix"> * </span></label>
                    <div class="col-md-7">
                        <input type='text' name='p_address' id='p_address'
                               required class='form-control '/>
                    </div>
                    <div class="col-md-1">

                    </div>
                </div>
{{-- 
                <div class="form-group  ">
                    <label for="Temporary Address" class=" control-label col-md-4 text-left"> Temporary Address</label>
                    <div class="col-md-7">
                        <input type='text' name='t_address' id='t_address' class='form-control '/>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div> --}}


                <div class="form-group  ">
                    <label for="Avatar" class=" control-label col-md-4 text-left"> Avatar </label>
                    <div class="col-md-7">
                        <input type='file' name='avatar' id='avatar' style='width:150px !important;'/>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
                <div class="form-group  ">
                    <label for="Gender" class=" control-label col-md-4 text-left"> Gender <span
                                class="asterix"> * </span></label>
                    <div class="col-md-7">

                        <label class='radio radio-inline'>
                            <input type='radio' name='gender' value='M' required> Male </label>
                        <label class='radio radio-inline'>
                            <input type='radio' name='gender' value='F' required> Female </label>
                    </div>
                    <div class="col-md-1">

                    </div>
                </div>
                <div class="form-group  ">
                    <label for="Qualification" class=" control-label col-md-4 text-left"> Qualification <span
                                class="asterix"> * </span></label>
                    <div class="col-md-7">
                        <select name='qualification' rows='5' id='qualification' class='select2 ' required>
                            @foreach($qualificationLevel as $row)
                                <option value="{{$row->id}}">{{$row->qualificationLevelName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-1">

                    </div>
                </div>
                <div class="form-group  ">
                    <label for="ContactNo" class=" control-label col-md-4 text-left"> ContactNo <span
                                class="asterix"> * </span></label>
                    <div class="col-md-7">
                        <input type='text' name='contactNo' id='contactNo'
                               required class='form-control '/>
                    </div>
                    <div class="col-md-1">

                    </div>
                </div>
            </fieldset>
        </div>


        <div style="clear:both"></div>


        <div class="form-group">
            <label class="col-sm-4 text-right">&nbsp;</label>
            <div class="col-sm-8">
                <button type="submit" name="submit" class="btn btn-primary btn-sm"><i
                            class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
            </div>

        </div>

        {!! Form::close() !!}
    </div>
@endsection		 
