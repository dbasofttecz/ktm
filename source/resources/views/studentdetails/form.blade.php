@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		</div>
		<div class="sbox-tools " >
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 			
		</div> 

	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'studentdetails/save?return='.$return, 'class'=>'form-vertical','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<ul class="nav nav-tabs"><li class="active"><a href="#Listitem1" data-toggle="tab">List item 1</a></li>
				<li class=""><a href="#Listitem2" data-toggle="tab">List item 2</a></li>
				<li class=""><a href="#Listitem3" data-toggle="tab">List item 3</a></li>
				</ul><div class="tab-content"><div class="tab-pane m-t active" id="Listitem1"> 
									
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Id    </label>									
										  <input  type='text' name='id' id='id' value='{{ $row['id'] }}' 
						     class='form-control ' /> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> First Name  <span class="asterix"> * </span>  </label>									
										  <input  type='text' name='first_name' id='first_name' value='{{ $row['first_name'] }}' 
						required     class='form-control ' /> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Last Name  <span class="asterix"> * </span>  </label>									
										  <input  type='text' name='last_name' id='last_name' value='{{ $row['last_name'] }}' 
						required     class='form-control ' /> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Username  <span class="asterix"> * </span>  </label>									
										  <input  type='text' name='username' id='username' value='{{ $row['username'] }}' 
						required     class='form-control ' /> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Password  <span class="asterix"> * </span>  </label>									
										  <input  type='text' name='password' id='password' value='{{ $row['password'] }}' 
						required     class='form-control ' /> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Avatar    </label>									
										  <input  type='file' name='avatar' id='avatar' @if($row['avatar'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['avatar'],'/uploads/studentPhotos/') !!}
						
						</div>					
					 						
									  </div> 
			</div>
			
			<div class="tab-pane m-t " id="Listitem2"> 
									
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Email  <span class="asterix"> * </span>  </label>									
										  <input  type='text' name='email' id='email' value='{{ $row['email'] }}' 
						required     class='form-control ' /> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Gender  <span class="asterix"> * </span>  </label>									
										  
					<label class='radio radio-inline'>
					<input type='radio' name='gender' value ='M' required @if($row['gender'] == 'M') checked="checked" @endif > Male </label>
					<label class='radio radio-inline'>
					<input type='radio' name='gender' value ='F' required @if($row['gender'] == 'F') checked="checked" @endif > Female </label> 						
									  </div> 
			</div>
			
			<div class="tab-pane m-t " id="Listitem3"> 
									
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Qualification  <span class="asterix"> * </span>  </label>									
										  <select name='qualification' rows='5' id='qualification' class='select2 ' required  ></select> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> ContactNo  <span class="asterix"> * </span>  </label>									
										  <input  type='text' name='contactNo' id='contactNo' value='{{ $row['contactNo'] }}' 
						required     class='form-control ' /> 						
									  </div> 
			</div>
			
			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('studentdetails?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#qualification").jCombo("{!! url('studentdetails/comboselect?filter=ktmimmig_qualification_level:id:qualificationLevelName') !!}",
		{  selected_value : '{{ $row["qualification"] }}' });
		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("studentdetails/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop