@extends('layouts.studentmaster')
@section('contents')
<?php $user = Auth::user(); ?>
@if($user->group_id == 4)
<div class="container">
    <div class="row">
       <div class="col-md-12">
					<form action="studentupdateprocess" method="post">
							{{ csrf_field() }}
						
					</form>
       </div>
    </div>
</div>
@else 
<div class="container text-center">
<div class="row">
<h2>You are not allowed here !!!</h2>
<a href="{{url('studentLogin')}}" class="btn btn-warning">Login</a>
<a href="{{url('/')}}" class="btn btn-warning">Go Back</a>
</div>
</div>
@endif
@endsection
