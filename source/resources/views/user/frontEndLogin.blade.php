@extends('layouts.frontendMaster')
@section('contents')
<div class="container text-center">
<div class="row ">
<div class="col-md-6 col-centered">
<div class="sbox ">
	<div class="sbox-title">
			
				<h3 >Login</h3>
				
	</div>
	<div class="sbox-content">
		<div class="ajaxLoading"></div>
		<p class="message alert alert-danger " style="display:none;"></p>	
 
	    	@if(Session::has('message'))
				{!! Session::get('message') !!}
			@endif
		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>		
	<div class="tab-content" >
		<div class="tab-pane active m-t" id="tab-sign-in">
		<form method="post" action="{{ url('studentloginprocess')}}" class="form-vertical" id="LoginAjax">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		
			<div class="form-group has-feedback animated fadeInLeft delayp1">
				<label>{{ Lang::get('core.email') }}	</label>
				<input type="text" name="email" placeholder="Email Address" class="form-control" required="email" />
				
				<i class="icon-users form-control-feedback"></i>
			</div>
			
			<div class="form-group has-feedback  animated fadeInRight delayp1">
				<label>{{ Lang::get('core.password') }}	</label>
				<input type="password" name="password" placeholder="Password" class="form-control" required="true" />
				
				<i class="icon-lock form-control-feedback"></i>
			</div>

			<div class="form-group has-feedback  animated fadeInRight delayp1">
				<label> Remember Me ?	</label>
				<input type="checkbox" name="remember" value="1" />
				
				<i class="icon-lock form-control-feedback"></i>
			</div>

			@if(CNF_RECAPTCHA =='true') 
			<div class="form-group has-feedback  animated fadeInLeft delayp1">
				<label class="text-left"> Are u human ? </label>	
				<br />
				{!! captcha_img() !!} <br /><br />
				<input type="text" name="captcha" placeholder="Type Security Code" class="form-control" required/>
				
				<div class="clr"></div>
			</div>	
		 	@endif	

			<div class="form-group  has-feedback text-center  animated fadeInLeft delayp1" style=" margin-bottom:20px;" >
				 	 
					<button type="submit" class="btn btn-info btn-sm btn-block" ><i class="fa fa-sign-in"></i> {{ Lang::get('core.signin') }}</button>
				       
			 	<div class="clr"></div>
				
			</div>		
		   </form>			


		{{-- forget passcode	 --}}
<a  class="mdoel-re-jatha" data-toggle="modal" data-target="#forgetpassword">{{ Lang::get('core.forgotpassword') }}</a>

<!-- Modal -->
<div id="forgetpassword" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('core.forgotpassword') }}</h4>
      </div>
      <div class="modal-body">
      


 	<form method="post" action="{{ url('user/request')}}" class="form-vertical box" id="fr">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		   <div class="form-group has-feedback">
		 
				<label>{{ Lang::get('core.enteremailforgot') }}</label>
				<input type="text" name="credit_email" placeholder="{{ Lang::get('core.email') }}" class="form-control" required/>
			<div class="text-center ktm-dba-softtech-woocodstudio">
			  <button type="submit" class="btn btn-default "> {{ Lang::get('core.sb_submit') }} </button>
			  </div>
			</div>
		  <div class="clr"></div>
		</form>






      </div>
    </div>

  </div>
</div>




		</div>
	</div>  

  </div>
</div>
</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#or').click(function(){
		$('#fr').toggle();
		});

		var form = $('#LoginAjax'); 
		form.parsley();
		form.submit(function(){
			
			if(form.parsley('isValid') == true){			
				var options = { 
					dataType:      'json', 
					beforeSubmit :  showRequest,
					success:       showResponse  
				}  
				$(this).ajaxSubmit(options); 
				return false;
							
			} else {
				return false;
			}		
		
		});

	});

function showRequest()
{
	$('.ajaxLoading').show();		
}  
function showResponse(data)  {		
	
	if(data.status == 'success')
	{
		window.location.href = data.url;	
		$('.ajaxLoading').hide();
	} else {
		$('.message').html(data.message)	
		$('.ajaxLoading').hide();
		$('.message').show(data.message)	
		return false;
	}	
}	
</script>
</div>
@endsection