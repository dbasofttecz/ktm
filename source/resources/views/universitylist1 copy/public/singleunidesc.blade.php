@extends('layouts.frontendMaster')
@section('contents')
<div class="container" style="padding-top:25px;">
	@foreach($data as $row)	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{$row->universityName}} </h3>
        <hr />       
    </div>
    @endforeach
</div>
<div class="container m-t">		
 	@foreach($data as $row)
 		{!! $row->universityDescription !!}
 	@endforeach
</div> 
@endsection