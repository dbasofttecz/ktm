@extends('layouts.frontendMaster')
@section('contents')
    <div class="container">
        <div class="row ">
            <h4 class="text-center">Recent Test Score Detail</h4>
            @foreach($data as $row)
                <div class="col-md-3">
                    <div class="all-recent-score-details">
                        <img src="{{url('uploads/recentScore/'.$row->studentPhoto)}}" class="img-responsive" ">
                        <h3>{!! $row->studentName !!}</h3>
                        <p>{!! $row->testScore !!}</p>
                        <p>{!! $row->testName !!}</p>
                        <p>{!! strip_tags($row->achievementDescription) !!}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection