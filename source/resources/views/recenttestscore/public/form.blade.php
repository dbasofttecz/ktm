

		 {!! Form::open(array('url'=>'recenttestscore/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Recent Test Score</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group  " >
										<label for="Student Name" class=" control-label col-md-4 text-left"> Student Name <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='studentName' id='studentName' value='{{ $row['studentName'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Test Score" class=" control-label col-md-4 text-left"> Test Score <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='testScore' id='testScore' value='{{ $row['testScore'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Test Name" class=" control-label col-md-4 text-left"> Test Name <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='testName' id='testName' value='{{ $row['testName'] }}' 
						required     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Achievemen tDescription" class=" control-label col-md-4 text-left"> Achievemen tDescription <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <textarea name='achievementDescription' rows='5' id='editor' class='form-control editor '  
						required >{{ $row['achievementDescription'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Student Photo" class=" control-label col-md-4 text-left"> Student Photo </label>
										<div class="col-md-7">
										  <input  type='file' name='studentPhoto' id='studentPhoto' @if($row['studentPhoto'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['studentPhoto'],'/uploads/recentScore/') !!}
						
						</div>					
					 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
