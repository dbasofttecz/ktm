<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Student Name</td>
						<td>{{ $row->studentName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Test Score</td>
						<td>{{ $row->testScore}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Test Name</td>
						<td>{{ $row->testName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Achievement Description</td>
						<td>{{ $row->achievementDescription}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Student Photo</td>
						<td>{!! SiteHelpers::formatRows($row->studentPhoto,$fields['studentPhoto'],$row ) !!} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	