<?php 
define('CNF_APPNAME','Ktm Immigration Pvt Ltd');
define('CNF_APPDESC','Ktm Immigration Consultancy');
define('CNF_COMNAME','Ktm Immigration Pvt Ltd');
define('CNF_EMAIL','info@ktmimmigration.edu.np');
define('CNF_METAKEY','ktm immigration, study, abroad study');
define('CNF_METADESC','Write description for your site');
define('CNF_GROUP','4');
define('CNF_ACTIVATION','confirmation');
define('CNF_MULTILANG','0');
define('CNF_LANG','en');
define('CNF_REGIST','true');
define('CNF_FRONT','true');
define('CNF_RECAPTCHA','false');
define('CNF_THEME','default');
define('CNF_RECAPTCHAPUBLICKEY','');
define('CNF_RECAPTCHAPRIVATEKEY','');
define('CNF_MODE','production');
define('CNF_LOGO','backend-logo.png');
define('CNF_ALLOWIP','');
define('CNF_RESTRICIP','192.116.134 , 194.111.606.21 ');
define('CNF_MAIL','phpmail');
define('CNF_DATE','m/d/y');
?>