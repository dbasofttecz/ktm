-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 31, 2017 at 08:31 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lmc`
--

-- --------------------------------------------------------

--
-- Table structure for table `study_level`
--

CREATE TABLE `study_level` (
  `id` int(11) NOT NULL,
  `studyLevelName` varchar(255) DEFAULT NULL,
  `studyDescription` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `study_level`
--

INSERT INTO `study_level` (`id`, `studyLevelName`, `studyDescription`) VALUES
(1, 'Under diploma', '<span style=\"color: rgb(75, 79, 86); font-family: \"San Francisco\", -apple-system, system-ui, \".SFNSText-Regular\", sans-serif; font-size: 12px; letter-spacing: -0.24px; white-space: pre-wrap; background-color: rgb(241, 240, 240);\">under dipoma desc</span>'),
(2, 'Diploma/ advance diploma', '<p><span style=\"color: rgb(75, 79, 86); font-family: \"San Francisco\", -apple-system, system-ui, \".SFNSText-Regular\", sans-serif; font-size: 12px; letter-spacing: -0.24px; white-space: pre-wrap; background-color: rgb(241, 240, 240);\">Diploma/ advance diploma desc</span><br></p>'),
(3, 'Undergraduate', '<p><span style=\"color: rgb(75, 79, 86); font-family: \"San Francisco\", -apple-system, system-ui, \".SFNSText-Regular\", sans-serif; font-size: 12px; letter-spacing: -0.24px; white-space: pre-wrap; background-color: rgb(241, 240, 240);\">Undergraduate</span><br></p>'),
(4, 'Postgraduate', '<p><span style=\"color: rgb(75, 79, 86); font-family: \"San Francisco\", -apple-system, system-ui, \".SFNSText-Regular\", sans-serif; font-size: 12px; letter-spacing: -0.24px; white-space: pre-wrap; background-color: rgb(241, 240, 240);\">Postgraduate</span><br></p>'),
(5, 'Masters', '<p><span style=\"color: rgb(75, 79, 86); font-family: \"San Francisco\", -apple-system, system-ui, \".SFNSText-Regular\", sans-serif; font-size: 12px; letter-spacing: -0.24px; white-space: pre-wrap; background-color: rgb(241, 240, 240);\">Masters</span><br></p>'),
(6, 'Language course', '<p><div class=\"_4tdt _ua0\" style=\"align-items: flex-end; display: flex; margin: 10px 9px 10px 8px; position: relative; justify-content: flex-end; font-family: \"San Francisco\", -apple-system, system-ui, \".SFNSText-Regular\", sans-serif; color: rgb(29, 33, 41); font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: -0.24px; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\"><div class=\"_ua2\" style=\"display: flex; flex: 1 1 auto; flex-direction: column; font-family: inherit;\"><div class=\"_4tdv\" style=\"font-family: inherit;\"><div class=\"_5wd4 _1nc6 direction_ltr\" style=\"direction: ltr; display: flex; flex-flow: row-reverse wrap; line-height: 1.28; padding: 0px 0px 1px; text-align: left; font-family: inherit;\"><div class=\"_h8t\" style=\"flex-direction: column; font-family: inherit;\"><div class=\"_5wd9\" data-tooltip-content=\"11:28am\" data-hover=\"tooltip\" data-tooltip-position=\"right\" style=\"min-height: 24px; font-family: inherit;\"><span style=\"font-family: inherit; background-color: rgb(241, 240, 240); color: rgb(75, 79, 86); white-space: pre-wrap; letter-spacing: -0.24px;\">Language course</span><br></div></div></div></div></div></div></p><div class=\"_4tdt _ua1\" style=\"align-items: flex-end; display: flex; margin: 10px 9px 10px 8px; position: relative; justify-content: flex-start; font-family: \"San Francisco\", -apple-system, system-ui, \".SFNSText-Regular\", sans-serif; color: rgb(29, 33, 41); font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: -0.24px; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\"><div class=\"_ua2\" style=\"display: flex; flex: 1 1 auto; flex-direction: column; font-family: inherit;\"><div class=\"_4tdv\" style=\"font-family: inherit;\"><div class=\"_5wd4 _1nc7 direction_ltr\" style=\"direction: ltr; display: flex; flex-wrap: wrap; line-height: 1.28; padding: 0px; text-align: left; font-family: inherit;\"><span class=\"_40fu\" style=\"cursor: pointer; float: none; align-items: center; display: flex; flex: 1 1 0%; font-family: inherit;\"><span class=\"_2u_d\" style=\"display: flex; position: static; top: 50%; transform: none; width: auto; margin: 0px 2px; font-family: inherit;\"></span></span></div></div></div></div>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `study_level`
--
ALTER TABLE `study_level`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `study_level`
--
ALTER TABLE `study_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
